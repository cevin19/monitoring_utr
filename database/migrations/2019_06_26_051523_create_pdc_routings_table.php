<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdcRoutingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_routings', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('table_name');
            $table->integer('routing_id')->unsigned();
            $table->integer('urutan');
            $table->foreign('routing_id')->references('id')->on('routings')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdc_routings');
    }
}
