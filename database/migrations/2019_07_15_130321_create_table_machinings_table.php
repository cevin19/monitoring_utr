<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMachiningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_machinings', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('nama_mesin');
            $table->string('register')->nullable();
            $table->string('pro_number')->nullable();
            $table->string('unit_model')->nullable();
            $table->string('nama_komponen')->nullable();
            $table->string('kondisi_mesin')->default('ready');
            $table->string('man_power')->nullable();
            $table->string('progress_job')->nullable();
            $table->datetime('start_progress')->nullable();
            $table->datetime('end_progress')->nullable();
            $table->string('laporan_keterlambatan')->nullable();
            $table->integer('routing_id')->nullable()->unsigned()->index();
            $table->boolean('done')->default(0);

            $table->foreign('routing_id')->references('id')->on('routings')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_machinings');
    }
}
