<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePartkitSatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_partkit_satus', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('register');
            $table->string('pro_number');
            $table->string('unit_model');
            $table->string('nama_komponen');
            $table->date('req_iserve')->nullable();
            $table->string('po_number')->nullable();
            $table->integer('std_release_po');
            $table->date('act_release_po')->nullable();
            $table->string('drawing_engineer');
            $table->string('status_drawing');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_partkit_satus');
    }
}
