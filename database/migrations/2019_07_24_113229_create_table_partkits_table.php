<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePartkitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_partkits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('register');
            $table->string('pro_number');
            $table->string('unit_model');
            $table->string('nama_komponen');
            $table->string('nama_mesin');
            $table->string('kondisi_mesin')->default('ready');
            $table->string('man_power');
            $table->integer('routing_id')->unsigned()->index();
            $table->timestamp('start_progress')->nullable();
            $table->timestamp('end_progress')->nullable();
            $table->boolean('done')->default(0);
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_partkits');
    }
}
