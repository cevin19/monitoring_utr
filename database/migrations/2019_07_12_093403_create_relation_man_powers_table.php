<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationManPowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_man_powers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('table_disassies_id')->unsigned()->index();
            $table->integer('man_power_id')->unsigned()->index();

            $table->foreign('table_disassy_id')->references('id')->on('table_disassies')->onDelete('cascade');
            $table->foreign('man_power_id')->references('id')->on('man_powers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relation_man_powers');
    }
}
