<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationMachinePausesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_machine_pauses', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('table_machining_id')->unsigned()->index();
            $table->integer('machine_pause_id')->unsigned()->index();

            $table->foreign('table_machining_id')->references('id')->on('table_machinings')->onDelete('cascade');
            $table->foreign('machine_pause_id')->references('id')->on('machine_pauses')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relation_machine_pauses');
    }
}
