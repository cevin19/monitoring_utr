<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePdcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_pdcs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('register');
            $table->string('unit_model');
            $table->string('nama_komponen');
            $table->date('date_in');
            $table->string('progress_job')->nullable();
            $table->datetime('start_progress')->nullable();;
            $table->date('date_out')->nullable();
            $table->boolean('done')->default(0);
            $table->text('laporan_keterlambatan')->nullable();
            $table->integer('routing_id')->unsigned();
            
            $table->foreign('routing_id')->references('id')->on('routings')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_pdcs');
    }
}
