
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssembliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_assemblies', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('register');
            $table->string('pro_number');
            $table->string('unit_model');
            $table->string('nama_komponen');
            $table->string('tube');
            $table->string('rod');
            $table->string('piston');
            $table->string('gland');
            $table->string('midle');
            $table->string('seal_kit');
            $table->string('bushing');
            $table->string('part_related');
            $table->string('status_komp');
            $table->string('estimasi_rfu');
            $table->string('skala_prioritas');
            $table->string('estimasi_assembly');
            $table->string('laporan_keterlambatan')->nullable();
            $table->boolean('done')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_assemblys');
    }
}
