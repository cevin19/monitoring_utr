<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/pdc','API\pdcController@return_pdc');
Route::get('/ppc','API\ppcController@return_ppc');
Route::get('/mkt','API\mktController@return_mkt');

// test api routing
Route::get('/pdc/show/{id}','API\pdcController@show_pdc');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
