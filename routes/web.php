<?php

Route::get('/user', 'User\userController@index');



// route for admin
Route::namespace('Admin')->middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('index.home');
    });
    Route::get('/admin-home', 'AdminController@index')->name('admin.home');
    // pdc
    Route::resource('/admin/PDC', 'pdcController');
    Route::patch('/admin/PDC/dateout/update/{id}', 'pdcController@update_date_out')->name('PDC.date-out.update');
    Route::patch('/admin/PDC/progress_job/update/{id}', 'pdcController@update_progress_job')->name('PDC.progress-job.update');
    Route::patch('/admin/PDC/startprogress/update/{id}', 'pdcController@update_start_progress')->name('PDC.start-progress.update');
    Route::patch('/admin/PDC/done/update/{id}', 'pdcController@update_done')->name('PDC.done.update');


    // mkt
    Route::resource('/admin/MKT', 'mktController');
    Route::patch('/admin/MKT/dateout/update/{id}', 'mktController@update_date_out')->name('MKT.date-out.update');
    Route::patch('/admin/MKT/progress_job/update/{id}', 'mktController@update_progress_job')->name('MKT.progress-job.update');
    Route::patch('/admin/MKT/startprogress/update/{id}', 'mktController@update_start_progress')->name('MKT.start-progress.update');
    Route::patch('/admin/MKT/done/update/{id}', 'mktController@update_done')->name('MKT.done.update');

    // ppc
    Route::resource('/admin/PPC', 'disassyController');
    Route::patch('/admin/PPC/dateout/update/{id}', 'disassyController@update_date_out')->name('PPC.date-out.update');
    Route::patch('/admin/PPC/progress_job/update/{id}', 'disassyController@update_progress_job')->name('PPC.progress-job.update');
    Route::patch('/admin/PPC/startprogress/update/{id}', 'disassyController@update_start_progress')->name('PPC.start-progress.update');
    Route::patch('/admin/PPC/done/update/{id}', 'disassyController@update_done')->name('PPC.done.update');

    //machine
    Route::resource('/admin/machining', 'machineController');
    Route::patch('/admin/machining/startprogress/update/{id}', 'machineController@update_start_progress')->name('machining.start-progress.update');
    Route::patch('/admin/machining/endprogress/update/{id}', 'machineController@update_end_progress')->name('machining.end-progress.update');
    Route::patch('/admin/newmachine', 'machineController@new_machine')->name('machining.new-machine');
    Route::get('/admin/machining/done/{id}','machineController@done')->name('machining.done');
    Route::get('newmachine','machineController@machine_create')->name('machine.create');
    Route::post('machine/store','machineController@machine_store')->name('machine.store');

    //partkit
    Route::resource('/admin/partkit', 'partkitController');

    //partkit_po
    Route::get('/admin/partkit-po/create', 'partkitController@create_po')->name('partkit.create_po');
    Route::post('/admin/partkit-po/store', 'partkitController@store_po')->name('partkit.store_po');
    Route::patch('/admin/partkit-po/actrelease/update/{id}', 'partkitController@update_act_release_po')->name('partkit_po.update.act_release_po');
    Route::patch('/admin/partkit-po/ponumber/update/{id}', 'partkitController@update_po_number')->name('partkit_po.update.po_number');


    //assembly
    Route::resource('/admin/assembly', 'assemblyController');
    Route::patch('/admin/assembly-update/tube/{id}', 'assemblyController@update_tube')->name('assembly.update.tube');
    Route::patch('/admin/assembly-update/rod/{id}', 'assemblyController@update_rod')->name('assembly.update.rod');
    Route::patch('/admin/assembly-update/piston/{id}', 'assemblyController@update_piston')->name('assembly.update.piston');
    Route::patch('/admin/assembly-update/gland/{id}', 'assemblyController@update_gland')->name('assembly.update.gland');
    Route::patch('/admin/assembly-update/midle/{id}', 'assemblyController@update_midle')->name('assembly.update.midle');
    Route::patch('/admin/assembly-update/seal_kit/{id}', 'assemblyController@update_seal_kit')->name('assembly.update.seal_kit');
    Route::patch('/admin/assembly-update/bushing/{id}', 'assemblyController@update_bushing')->name('assembly.update.bushing');
    Route::patch('/admin/assembly-update/part_related/{id}', 'assemblyController@update_part_related')->name('assembly.update.part_related');

    // delay
    Route::resource('admin/delay', 'delayController');
    Route::patch('admin/delay/unpause/{id}', 'delayController@unpause')->name('machining.unpause');

    // routing
    Route::resource('/admin/routing', 'routingController');
    Route::post('/admin/routing/excel', 'routingController@excel')->name('routing.excel');
    Route::get('/admin/routing-edit/pdc', 'routingController@routing_pdc')->name('routing-edit.pdc');
    Route::post('/admin/routing-edit/pdc', 'routingController@routing_pdc_store')->name('routing-store.pdc');
    Route::delete('/admin/routing-edit/pdc/{id}', 'routingController@routing_pdc_delete')->name('relation_routing.destroy.pdc');
    Route::post('/admin/routing-edit/pdcstore', 'routingController@routing_pdc_store')->name('relation_routing.store.pdc');

    // routing ppc
    Route::get('/admin/routing-edit/ppc', 'routingController@routing_ppc')->name('routing-edit.ppc');
    Route::post('/admin/routing-edit/ppc', 'routingController@routing_ppc_store')->name('routing-store.ppc');
    Route::delete('/admin/routing-edit/ppc/{id}', 'routingController@routing_ppc_delete')->name('relation_routing.destroy.ppc');
    Route::post('/admin/routing-edit/ppcstore', 'routingController@routing_ppc_store')->name('relation_routing.store.ppc');

    //routing mkt
    Route::get('/admin/routing-edit/mkt', 'routingController@routing_mkt')->name('routing-edit.mkt');
    Route::post('/admin/routing-edit/mkt', 'routingController@routing_mkt_store')->name('routing-store.mkt');
    Route::delete('/admin/routing-edit/mkt/{id}', 'routingController@routing_mkt_delete')->name('relation_routing.destroy.mkt');
    Route::post('/admin/routing-edit/mktstore', 'routingController@routing_mkt_store')->name('relation_routing.store.mkt');

    // history
    Route::resource('/admin/history', 'historyController');

    // 
    Route::get('/pdc','userController@index')->name('user.pdc');
    Route::get('/station1', function () {
        return view('station1.home');
    });
});


Auth::routes();
