<?php

namespace App\Imports;

use App\Model\routing;
use Maatwebsite\Excel\Concerns\ToModel;

class RoutingImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        
        return new routing([
            
            'grup'  => $row[0],
            'model_unit'  => $row[1],
            'komponen'  => $row[2],
            'sub_komponen'  => $row[3],
            'kategori_kerusakan'  => $row[4],
            'activity'  => $row[5],
            'estimasi'  => $row[6],
        ]);
    }
}
