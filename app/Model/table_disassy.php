<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class table_disassy extends Model
{
    public function man_powers()
    {
        return $this->belongsToMany('App\Model\man_power','relation_man_powers');
    }
}
