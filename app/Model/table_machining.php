<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class table_machining extends Model
{
    public function routings()
    {
        return $this->hasOne('App\Model\routing','id','routing_id');
    }

    public function machine_pauses()
    {
        return $this->belongsToMany('App\Model\machine_pause', 'relation_machine_pauses');
    }
}
