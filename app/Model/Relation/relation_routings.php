<?php

namespace App\Model\Relation;

use Illuminate\Database\Eloquent\Model;

class relation_routings extends Model
{
    protected $fillable =['table_name','routing_id','urutan'];

    public function routings()
    {
        return $this->belongsTo('App\Model\routing');
    }
}
