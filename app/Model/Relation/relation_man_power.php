<?php

namespace App\Model\Relation;

use Illuminate\Database\Eloquent\Model;

class relation_man_power extends Model
{
    public function man_powers()
    {
        return $this->belongsTo('App\Model\man_power');
    }

    public function table_dissasies()
    {
        return $this->belongsTo('App\Model\table_dissasy');
    }
}
