<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class machine_pause extends Model
{
    public function table_machinings()
    {
        return $this->belongsToMany('App\Model\table_machining', 'relation_machine_pauses');
    }
}
