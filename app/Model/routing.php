<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class routing extends Model
{

    protected $fillable=['grup','model_unit','komponen','sub_komponen','kategori_kerusakan','activity','estimasi','created_at','updated_at'];
    public function table_pdcs()
    {
        return $this->belongsToMany('App\Model\table_pdc','pdc_routings');
    }

    public function table_machining()
    {
        return $this->hasMany('App\Model\table_machining');
    }
}
