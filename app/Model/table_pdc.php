<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class table_pdc extends Model
{
    public function routings()
    {
        return $this->belongsToMany('App\Model\routing', 'pdc_routings');
    }
}
