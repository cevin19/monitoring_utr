<?php

namespace App\Exports;

use App\routing;
use Maatwebsite\Excel\Concerns\FromCollection;

class RoutingExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return routing::all();
    }
}
