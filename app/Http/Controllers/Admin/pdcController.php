<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// use model
use App\Model\table_pdc;
use App\Model\routing;
use App\Model\std_leadtime;
use App\Model\Relation\relation_routings;

use Carbon\Carbon;


class pdcController extends Controller
{
    public function index()
    {
        $pdcs   = table_pdc::all();
        $routings  = routing::all();
        $std_leadtimes = std_leadtime::all();
        $relation_routings  = relation_routings::where('table_name', 'pdc')->get();

        return view('admin.pdc.index', compact('pdcs', 'routings', 'relation_routings', 'std_leadtimes'));
    }

    public function create()
    {
        $routings  = routing::all();
        $relation_routings  = relation_routings::where('table_name', 'pdc')->get();
        return view('admin.pdc.create', compact('routings', 'relation_routings'));
    }

    public function store(Request $request)
    {

        $rules = [
            'register'              => 'required|',
            'unit_model'            => 'required',
            'nama_komponen'         => 'required',
            'date_in'               => 'required',
            'routing_id'          => 'required',

        ];

        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];

        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([]);

        $routing = routing::find($request->routing_id);

        $pdc = new table_pdc;

        $pdc->register              = $request->register;
        $pdc->unit_model            = $request->unit_model;
        $pdc->nama_komponen         = $request->nama_komponen;
        $pdc->date_in               = $request->date_in;
        $pdc->progress_job          = $routing->activity;
        $pdc->start_progress        = $request->start_progress;
        $pdc->date_out              = $request->date_out;
        $pdc->routing_id              = $routing->id;

        $pdc->save();

        return redirect(route('PDC.index'));
    }

    public function show($id)
    {

        $pdc = table_pdc::find($id)->routings();
        $pdc_routing = pdc_routing::where('table_pdc_id', $id)->orderBy('urutan')->get();
        $routings = routing::all();



        return view('admin.pdc.show', compact('pdc', 'pdc_routing', 'routings'));
    }

    public function edit($id)
    {
        $pdc = table_pdc::find($id);
        $routings = routing::all();

        return view('admin.pdc.edit', compact('pdc', 'routings'));
    }

    public function update(Request $request, $id)
    {

        $rules = [
            'register'              => 'required|',
            'unit_model'            => 'required',
            'nama_komponen'         => 'required',
            'date_in'               => 'required',
            'progress_job'          => 'required',

        ];

        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];

        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([]);

        $pdc =  table_pdc::find($id);

        $pdc->register              = $request->register;
        $pdc->unit_model            = $request->unit_model;
        $pdc->nama_komponen         = $request->nama_komponen;
        $pdc->date_in               = $request->date_in;
        $pdc->progress_job          = $request->progress_job;
        $pdc->start_progress        = $request->start_progress;
        $pdc->date_out              = $request->date_out;

        $pdc->update();

        return redirect(route('PDC.index'));
    }

    public function update_progress_job(Request $request, $id)
    {
        $data = table_pdc::find($id);
        $data->progress_job = $request->activity;
        $data->routing_id = $request->routing_id;
        $data->update();
        return redirect()->back();
    }

    public function update_date_out(Request $request, $id)
    {

        $data = table_pdc::find($id);
        $data->date_out = $request->date_out;
        $data->update();
        return redirect()->back();
    }

    public function update_start_progress(Request $request, $id)
    {
        $data = table_pdc::find($id);
        $data->start_progress = Carbon::now();
        $data->update();
        return redirect()->back();
    }

    public function update_done(Request $request, $id)
    {
        $data = table_pdc::find($id);
        $data->done = 1;
        $data->laporan_keterlambatan = $request->laporan_keterlambatan;
        $data->update();
        return redirect()->back();
    }

    public function destroy($id)
    {
        $pdc = table_pdc::find($id);
        $pdc->delete();

        return redirect()->back();
    }
}
