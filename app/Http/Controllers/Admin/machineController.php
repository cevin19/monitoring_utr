<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\table_machining;
use App\Model\routing;
use App\Model\man_power;
use App\Model\Relation\relation_machine_pause;
use Carbon\Carbon;

class machineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $machinings = table_machining::where('done',0)->get()->sortBy('nama_mesin');
        $routings   = routing::all();
        $pause      = relation_machine_pause::all();

        return view('admin.machine.index', compact('machinings', 'routings', 'pause'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $routings   = routing::all();
        $man_powers = man_power::where('status', 'ready')->get();
        $machines   = table_machining::where('done',0)->get();
        return view('admin.machine.create', compact('routings', 'man_powers', 'machines'));
    }

    public function machine_create(){
        $machines  = table_machining::all()->sortBy('nama_mesin');
        return view('admin.machine.create-machine',compact('machines'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }
    public function machine_store(Request $request){
        $rules = [
            'nama_mesin'              => 'required',
        ];

        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];

        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([]);

        $machine = new table_machining;
        $machine->nama_mesin = $request->nama_mesin;
        $machine->save();
        return redirect()->back();
    }
    public function new_machine(Request $request)
    {
        $man_power  = man_power::find($request->man_power);
        $routing    = routing::where([
            'model_unit' => $request->unit_model,
            'komponen' => $request->komponen,
            'sub_komponen' => $request->sub_komponen,
            'kategori_kerusakan' => $request->kategori_kerusakan,
            'activity' => $request->progress_job
        ])->get()->first();

        $machine = table_machining::find($request->machine_id);
        $machine->register          = $request->register;
        $machine->pro_number        = $request->pro_number;
        $machine->unit_model        = $request->unit_model;
        $machine->nama_komponen     = $request->komponen;
        $machine->kondisi_mesin     = 'runing';
        $machine->man_power         = $man_power->nama;
        $machine->progress_job      = $request->progress_job;
        $machine->start_progress    = $request->start_progress;
        $machine->routing_id        = $routing->id;
        $machine->update();
        $man_power->status          = 'onjob';
        $man_power->update();

        return redirect(route('machining.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $machine = machine::find($id);
        return view('admin.machine.show', compact('machine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plants     = plant::all();
        $machine    = machine::find($id);
        return view('admin.machine.edit', compact('machine', 'plants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $machine = table_machining::find($id);

        $machine->name         = $request->name;
        $machine->damage_type  = $request->damage_type;
        $machine->date_in      = $request->date_in;
        $machine->plant_id     = $request->plant_id;

        $machine->update();

        return redirect(route('machine.index'));
    }

    public function update_start_progress(Request $request, $id)
    {
        $data = table_machining::find($id);
        $data->start_progress = $request->start_progress;
        $data->update();
        return redirect()->back();
    }

    public function update_end_progress(Request $request, $id)
    {
        $data = table_machining::find($id);
        $data->end_progress = Carbon::now();
        $data->update();
        return redirect()->back();
    }

    public function done($id){
        $data = table_machining::find($id);
        $man_power = man_power::where('nama',$data->man_power)->get()->first();
        $man_power->status = 'ready';
        $man_power->update();
        $data->done = 1;
        $data->update();
        $machine = new table_machining;
        $machine->nama_mesin = $data->nama_mesin;
        $machine->save();
        return redirect(route('machining.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $machine = table_machining::find($id);
        $machine->delete();

        return redirect()->back();
    }
}
