<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// use model
use App\Model\table_disassy;
use App\Model\routing;
use App\Model\std_leadtime;
use App\Model\Relation\relation_routings;
use App\Model\man_power;

class disassyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ppcs       = table_disassy::all(); 
        $routings   = routing::all();
        $std_leadtimes = std_leadtime::all();
        $relation_routings  = relation_routings::where('table_name','ppc')->get();

        return view('admin.ppc.index',compact('ppcs','routings','std_leadtimes','relation_routings'));
        
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $routings           = routing::all();
        $relation_routings  = relation_routings::where('table_name','ppc')->get();
        $man_powers         = man_power::all();
        return view('admin.ppc.create',compact('routings','relation_routings','man_powers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $rules = [
            'register'              => 'required|',
            'pro_number'            => 'required', 
            'unit_model'            => 'required', 
            'nama_komponen'         => 'required',            
            'date_in'               => 'required',            
            'progress_job'          => 'required',   
        ];
    
        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];
    
        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([
            
        ]);

        $routing = routing::find($request->progress_job);
        
        $ppc = new table_disassy;

        $ppc->register              = $request->register;
        $ppc->pro_number            = $request->pro_number;
        $ppc->unit_model            = $request->unit_model;
        $ppc->nama_komponen         = $request->nama_komponen;
        $ppc->date_in               = $request->date_in;
        $ppc->progress_job          = $routing->activity;
        $ppc->start_progress        = $request->start_progress;
        $ppc->date_out              = $request->date_out;
        $ppc->remarks_keterlambatan = '';
        $ppc->routing_id            = $routing->id;

        $ppc->save();

        $ppc->man_powers()->sync($request->man_powers);

        return redirect(route('PPC.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function update_progress_job(Request $request, $id)
    {
        $data = table_disassy::find($id);
        $data->progress_job = $request->activity;
        $data->progress_job = $request->routing_id;
        $data->update();
        return redirect()->back();
    }

    public function update_date_out(Request $request,$id){
        
        $data = table_disassy::find($id);
        $data->date_out = $request->date_out;
        $data->update();
        return redirect()->back();
    }

    public function update_start_progress(Request $request,$id){
        $data = table_disassy::find($id);
        $data->start_progress = $request->start_progress;
        $data->update();
        return redirect()->back();
    }

    public function update_done(Request $request, $id){
        $data = table_disassy::find($id);
        $data->done = 1;
        $data->remark_keterlambatan = $request->remark_keterlambatan;
        $data->update();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ppc = table_disassy::find($id);
        $ppc->delete();

        return redirect()->back();
    }
}
