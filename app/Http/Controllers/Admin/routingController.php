<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// upload excel
use Excel;
use App\Imports\RoutingImport;
// model
use App\Model\routing;
use App\Model\Relation\relation_routings;


class routingController extends Controller
{
    public function index()
    {
        $routings   = routing::all();
        return view('admin.routing.index', compact('routings'));
    }


    public function create()
    {
        return view('admin.routing.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'grup'                 => 'required|',
            'model_unit'           => 'required',
            'komponen'             => 'required',
            'sub_komponen'         => 'required',
            'kategori_kerusakan'   => 'required',
            'activity'             => 'required',
            'estimasi'             => 'required',

        ];

        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];

        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([]);

        $routing = new routing;

        $routing->grup                  = $request->grup;
        $routing->model_unit            = $request->model_unit;
        $routing->komponen              = $request->komponen;
        $routing->sub_komponen          = $request->sub_komponen;
        $routing->kategori_kerusakan    = $request->kategori_kerusakan;
        $routing->activity              = $request->activity;
        $routing->estimasi              = $request->estimasi;

        $routing->save();

        return redirect(route('routing.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $routing = routing::find($id);
        $routing->delete();

        return redirect()->back();
    }

    public function excel(Request $request)
    {

        $this->validate($request, [
            'file'    => 'required|mimes:xls,xlsx,ods',
        ]);

        // menangkap file excel
        $file = $request->file('file');
        // // membuat nama file unik
        // $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        // $file->move('file_routing', $nama_file);

        // import data
        Excel::import(new RoutingImport, $file);

        return redirect(route('routing.index'));
    }

    public function routing_pdc()
    {
        $routings = routing::all();
        $relation_routings = relation_routings::where('table_name', 'pdc')->get();
        return view('admin.pdc.routing-edit', compact('routings', 'relation_routings'));
    }
    public function routing_pdc_delete($id)
    {

        $relation_routings = relation_routings::where('table_name', 'pdc')->get();
        $data = relation_routings::find($id);
        foreach ($relation_routings as $relation_routing) {
            if ($relation_routing->urutan > $data->urutan) {
                $relation_routing_update = relation_routings::find($relation_routing->id);
                $relation_routing_update->urutan = $relation_routing_update->urutan - 1;
                $relation_routing_update->update();
            }
        }
        $data->delete();
        return redirect()->back();
    }

    public function routing_pdc_store(Request $request)
    {
        $relation_routings = relation_routings::where('table_name', 'pdc')->get();
        $routing           = routing::where([
            'model_unit'=>$request->unit_model,
            'komponen'=>$request->komponen,
            'sub_komponen'=>$request->sub_komponen,
            'kategori_kerusakan'=>$request->kategori_kerusakan,
            'activity'=>$request->progress_job,
            ])->get()->first();

        $data = new relation_routings;
        $data->table_name   = 'pdc';
        $data->routing_id   = $routing->id;
        $data->urutan       = count($relation_routings) + 1;
        $data->save();
        return redirect()->back();
    }

    // mkt
    public function routing_mkt()
    {
        $routings = routing::all();
        $relation_routings = relation_routings::where('table_name', 'mkt')->get();
        return view('admin.mkt.routing-edit', compact('routings', 'relation_routings'));
    }
    public function routing_mkt_delete($id)
    {

        $relation_routings = relation_routings::where('table_name', 'mkt')->get();
        $data = relation_routings::find($id);
        foreach ($relation_routings as $relation_routing) {
            if ($relation_routing->urutan > $data->urutan) {
                $relation_routing_update = relation_routings::find($relation_routing->id);
                $relation_routing_update->urutan = $relation_routing_update->urutan - 1;
                $relation_routing_update->update();
            }
        }
        $data->delete();
        return redirect()->back();
    }

    public function routing_mkt_store(Request $request)
    {
        $relation_routings = relation_routings::where('table_name', 'mkt')->get();
        $routing           = routing::where([
            'model_unit'=>$request->unit_model,
            'komponen'=>$request->komponen,
            'sub_komponen'=>$request->sub_komponen,
            'kategori_kerusakan'=>$request->kategori_kerusakan,
            'activity'=>$request->progress_job,
            ])->get()->first();
        $data = new relation_routings;
        $data->table_name   = 'mkt';
        $data->routing_id   = $routing->id;
        $data->urutan       = count($relation_routings) + 1;
        $data->save();
        return redirect()->back();
    }

    // ppc

    public function routing_ppc()
    {
        $routings = routing::all();
        $relation_routings = relation_routings::where('table_name', 'ppc')->get();
        return view('admin.ppc.routing-edit', compact('routings', 'relation_routings'));
    }
    public function routing_ppc_delete($id)
    {

        $relation_routings = relation_routings::where('table_name', 'ppc')->get();
        $data = relation_routings::find($id);
        foreach ($relation_routings as $relation_routing) {
            if ($relation_routing->urutan > $data->urutan) {
                $relation_routing_update = relation_routings::find($relation_routing->id);
                $relation_routing_update->urutan = $relation_routing_update->urutan - 1;
                $relation_routing_update->update();
            }
        }
        $data->delete();
        return redirect()->back();
    }

    public function routing_ppc_store(Request $request)
    {
        $relation_routings = relation_routings::where('table_name', 'ppc')->get();

        $routing           = routing::where([
            'model_unit'=>$request->unit_model,
            'komponen'=>$request->komponen,
            'sub_komponen'=>$request->sub_komponen,
            'kategori_kerusakan'=>$request->kategori_kerusakan,
            'activity'=>$request->progress_job,
            ])->get()->first();
        $data = new relation_routings;
        $data->table_name   = 'ppc';
        $data->routing_id   = $routing->id;
        $data->urutan       = count($relation_routings) + 1;
        $data->save();
        return redirect()->back();
    }
}
