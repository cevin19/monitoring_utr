<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// use model
use App\Model\table_mkt;
use App\Model\routing;
use App\Model\std_leadtime;
use App\Model\Relation\relation_routings;


class mktController extends Controller
{
    public function index()
    {
        $mkts   = table_mkt::all();
        $routings  = routing::all();
        $std_leadtimes = std_leadtime::all();
        $relation_routings  = relation_routings::where('table_name', 'mkt')->get();

        return view('admin.mkt.index', compact('mkts', 'routings', 'relation_routings', 'std_leadtimes'));
    }

    public function create()
    {
        $routings  = routing::all();
        $relation_routings  = relation_routings::where('table_name', 'mkt')->get();
        return view('admin.mkt.create', compact('routings', 'relation_routings'));
    }

    public function store(Request $request)
    {

        $rules = [
            'register'              => 'required|',
            'unit_model'            => 'required',
            'nama_komponen'         => 'required',
            'date_in'               => 'required',
            'progress_job'          => 'required',

        ];

        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];

        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([]);

        $routing = routing::find($request->progress_job);
        $mkt = new table_mkt;

        $mkt->register              = $request->register;
        $mkt->unit_model            = $request->unit_model;
        $mkt->nama_komponen         = $request->nama_komponen;
        $mkt->date_in               = $request->date_in;
        $mkt->progress_job          = $routing->activity;
        $mkt->start_progress        = $request->start_progress;
        $mkt->date_out              = $request->date_out;
        $mkt->routing_id              = $routing->id;

        $mkt->save();

        return redirect(route('MKT.index'));
    }

    public function show($id)
    {

        $mkt = table_mkt::find($id)->routings();
        $mkt_routing = mkt_routing::where('table_mkt_id', $id)->orderBy('urutan')->get();
        $routings = routing::all();



        return view('admin.mkt.show', compact('mkt', 'mkt_routing', 'routings'));
    }

    public function edit($id)
    {
        $mkt = table_mkt::find($id);
        $routings = routing::all();

        return view('admin.mkt.edit', compact('mkt', 'routings'));
    }

    public function update(Request $request, $id)
    {

        $rules = [
            'register'              => 'required|',
            'unit_model'            => 'required',
            'nama_komponen'         => 'required',
            'date_in'               => 'required',
            'progress_job'          => 'required',

        ];

        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];

        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([]);

        $mkt =  table_mkt::find($id);

        $mkt->register              = $request->register;
        $mkt->unit_model            = $request->unit_model;
        $mkt->nama_komponen         = $request->nama_komponen;
        $mkt->date_in               = $request->date_in;
        $mkt->progress_job          = $request->progress_job;
        $mkt->start_progress        = $request->start_progress;
        $mkt->date_out              = $request->date_out;

        $mkt->update();

        return redirect(route('MKT.index'));
    }

    public function update_progress_job(Request $request, $id)
    {
        $data = table_mkt::find($id);
        $data->progress_job = $request->activity;
        $data->progress_job = $request->routing_id;
        $data->update();
        return redirect()->back();
    }

    public function update_date_out(Request $request, $id)
    {

        $data = table_mkt::find($id);
        $data->date_out = $request->date_out;
        $data->update();
        return redirect()->back();
    }

    public function update_start_progress(Request $request, $id)
    {
        $data = table_mkt::find($id);
        $data->start_progress = $request->start_progress;
        $data->update();
        return redirect()->back();
    }

    public function update_done(Request $request, $id)
    {
        $data = table_mkt::find($id);
        $data->done = 1;
        $data->laporan_keterlambatan = $request->laporan_keterlambatan;
        $data->update();
        return redirect()->back();
    }

    public function destroy($id)
    {
        $mkt = table_mkt::find($id);
        $mkt->delete();

        return redirect()->back();
    }
}
