<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\table_partkit_satu;
use App\Model\table_partkit;
use App\Model\routing;
use Carbon\Carbon;

class partkitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partkit_satus  = table_partkit_satu::all();
        $partkits        = table_partkit::all();
        $routings       = routing::all();
        return view('admin.partkit.index', compact('partkit_satus','partkits','routings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partkit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // PO PARTKIT

    public function create_po()
    {
        return view('admin.partkit.create_po');
    }

    public function store_po(Request $request)
    {

        $rules = [
            'register'              => 'required|',
            'unit_model'            => 'required',
            'nama_komponen'         => 'required',
            'std_release_po'        => 'required',
            'pro_number'            => 'required',

        ];

        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];

        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([]);

        $partkit_satus = new table_partkit_satu;

        $partkit_satus->register              = $request->register;
        $partkit_satus->pro_number            = $request->pro_number;
        $partkit_satus->unit_model            = $request->unit_model;
        $partkit_satus->nama_komponen         = $request->nama_komponen;
        $partkit_satus->req_iserve            = $request->req_iserve;
        $partkit_satus->po_number             = $request->po_number;
        $partkit_satus->std_release_po        = $request->std_release_po;
        $partkit_satus->drawing_engineer      = $request->drawing_engineer;
        $partkit_satus->status_drawing        = $request->status_drawing;

        $partkit_satus->save();

        return redirect(route('partkit.index'));
    }
    public function update_act_release_po(Request $request, $id)
    {
        $data = table_partkit_satu::find($id);
        $data->act_release_po = $request->act_release_po;
        $data->update();
        return redirect()->back();
    }
    public function update_po_number(Request $request, $id)
    {
        $data = table_partkit_satu::find($id);
        $data->po_number = $request->po_number;
        $data->update();
        return redirect()->back();
    }
}
