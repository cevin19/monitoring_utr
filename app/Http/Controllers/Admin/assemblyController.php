<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\table_assembly;
use App\Model\routing;

class assemblyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assemblies   = table_assembly::all();
        $routings   = routing::all();
        return view("admin.assembly.index", compact('assemblies', 'routings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $routings   = routing::all();
        return view("admin.assembly.create", compact('routings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'register'              => 'required|',
            'pro_number'            => 'required',
            'model_unit'            => 'required',
            'nama_komponen'         => 'required',
            'status_komp'           => 'required',
            'estimasi_rfu'          => 'required',
            'skala_prioritas'       => 'required',
            'estimasi_assembly'     => 'required',

        ];

        $customMessages = [
            'required' => 'Kolom :attribute Harus diisi'
        ];

        $this->validate($request, $rules, $customMessages);
        $validatedData = $request->validate([]);

        $assembly = new table_assembly;

        $assembly->register              = $request->register;
        $assembly->pro_number            = $request->pro_number;
        $assembly->unit_model            = $request->model_unit;
        $assembly->nama_komponen         = $request->nama_komponen;
        $assembly->status_komp           = $request->status_komp;
        $assembly->estimasi_rfu          = $request->estimasi_rfu;
        $assembly->skala_prioritas       = $request->skala_prioritas;
        $assembly->estimasi_assembly     = $request->estimasi_assembly;
        $assembly->tube                  = $request->tube;
        $assembly->rod                   = $request->rod;
        $assembly->piston                = $request->piston;
        $assembly->gland                 = $request->gland;
        $assembly->midle                 = $request->midle;
        $assembly->seal_kit              = $request->seal_kit;
        $assembly->bushing               = $request->bushing;
        $assembly->part_related          = $request->part_related;


        $assembly->save();

        return redirect(route('assembly.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update_tube(Request $request, $id)
    {
        $data = table_assembly::find($id);
        $data->tube = $request->tube;
        $data->update();
        return redirect()->back();
    }

    public function update_rod(Request $request, $id)
    {
        $data = table_assembly::find($id);
        $data->rod = $request->rod;
        $data->update();
        return redirect()->back();
    }

    public function update_piston(Request $request, $id)
    {
        $data = table_assembly::find($id);
        $data->piston = $request->piston;
        $data->update();
        return redirect()->back();
    }

    public function update_gland(Request $request, $id)
    {
        $data = table_assembly::find($id);
        $data->gland = $request->gland;
        $data->update();
        return redirect()->back();
    }

    public function update_midle(Request $request, $id)
    {
        $data = table_assembly::find($id);
        $data->midle = $request->midle;
        $data->update();
        return redirect()->back();
    }

    public function update_seal_kit(Request $request, $id)
    {
        $data = table_assembly::find($id);
        $data->seal_kit = $request->seal_kit;
        $data->update();
        return redirect()->back();
    }

    public function update_bushing(Request $request, $id)
    {
        $data = table_assembly::find($id);
        $data->bushing = $request->bushing;
        $data->update();
        return redirect()->back();
    }

    public function update_part_related(Request $request, $id)
    {
        $data = table_assembly::find($id);
        $data->part_related = $request->part_related;
        $data->update();
        return redirect()->back();
    }
}
