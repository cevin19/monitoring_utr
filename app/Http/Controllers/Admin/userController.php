<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\table_pdc;
use App\Model\std_leadtime;
use App\Model\routing;
use App\Model\Relation\relation_routings;

class userController extends Controller
{
    public function index()
    {
        $pdcs = table_pdc::all();
        $routings  = routing::all();
        $std_leadtimes = std_leadtime::all();
        $relation_routings  = relation_routings::where('table_name', 'pdc')->get();
        return view('station1.halaman.pdc', compact('pdcs', 'std_leadtimes', 'relation_routings', 'routings'));
    }
}
