<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\table_pdc;

class pdcController extends Controller
{
    public function return_pdc(){
        return table_pdc::all();
    }
    public function show_pdc($id){
        return table_pdc::find($id);
    }
}
