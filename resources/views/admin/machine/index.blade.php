@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Index Machining</strong>
        @if(Auth::user()->role == 'admin')
        <a class="btn btn-danger float-right " style="margin-left:2px" href="{{route('machining.create')}}">Tambah Data
            Machining</a>
        <a class="btn btn-warning float-right " style="margin-left:2px" href="{{route('machine.create')}}">Edit Machine</a>
        @endif
    </div>

    <div class="card-body">
        <div class="table-stats ov-h">
            <table class="" style="text-align:center" id="machining" class="display">
                <thead>
                    <tr style="background-color:#e8e9ef">
                        <th>nama&nbsp;mesin</th>
                        <th>register</th>
                        <th>PRO Number</th>
                        <th>unit model</th>
                        <th>nama komponen</th>
                        <th>man power</th>
                        <th>progress job</th>
                        <th>kategori job</th>
                        <th>Std Routing(jam)</th>
                        <th>Start Progress</th>
                        <th>Estimasi Finish</th>
                        <th>Aktual Finish</th>
                        <th>Real Time</th>
                        <th>Status Time Pekerjaan</th>
                        <th>Aksi</th>

                    </tr>
                </thead>

                <tbody>
                    @foreach($machinings as $machining)
                    @foreach ($routings as $routing)
                    @if($routing->id == $machining->routing_id)

                    {{-- data yang dipake --}}
                    @php
                    $waktu_sekarang = \Carbon\Carbon::now();
                    $waktu_asal = $machining->start_progress;
                    $waktu_total = \Carbon\Carbon::parse($waktu_asal);
                    $waktu_total = $waktu_total->addMinutes(($routing->estimasi)*60);

                    $pauses =
                    App\Model\Relation\relation_machine_pause::where('table_machining_id',$machining->id)->get()->all();
                    $machine_pauses = App\Model\machine_pause::whereNotNull('end_pause')->get()->all();

                    $total_pause = 0;
                    if($pauses != null){
                    foreach($pauses as $pause){
                    foreach($machine_pauses as $machine_pause){
                    if($machine_pause->id == $pause->machine_pause_id){
                    $start_pause = $machine_pause->start_pause;
                    $end_pause = $machine_pause->end_pause;
                    $timeFirst = strtotime($start_pause);
                    $timeSecond = strtotime($end_pause);
                    $differenceInSeconds = $timeSecond - $timeFirst;

                    $total_pause += $differenceInSeconds;

                    }
                    }
                    }
                    }
                    $machine_pause3 =null;
                    $machine_pauses2 = App\Model\machine_pause::whereNull('end_pause')->get()->all();
                    foreach($machine_pauses2 as $machine_pause2){
                    foreach($pauses as $pause){
                    if($pause->machine_pause_id == $machine_pause2->id){
                    $machine_pause3 += 1;
                    $machine_pause4 = $machine_pause2;
                    }
                    }
                    }

                    @endphp

                    <tr>
                        <td>{{$machining->nama_mesin}}</td>
                        <td>{{$machining->register}}</td>
                        <td>{{$machining->pro_number}}</td>
                        <td>{{$machining->unit_model}}</td>
                        <td>{{$machining->nama_komponen}}</td>
                        <td>{{$machining->man_power}}</td>
                        <td>
                            {{$routing->activity}}
                        </td>
                        <td>
                            {{$routing->kategori_kerusakan}}
                        </td>

                        <td>
                            {{$routing->estimasi}}
                        </td>
                        @if($machining->start_progress != null)
                        <td nowrap="nowrap">
                            @php
                            $start_progress = \Carbon\Carbon::parse($machining->start_progress)->format('d-M-y
                            H:i:s');
                            @endphp
                            {{
                            substr($start_progress,0,10)
                            }}
                            <br>
                            {{
                            substr($start_progress,10)
                            }}
                        </td>
                        @else
                        <form id="form-update-start-progress-{{$machining->id}}"
                            action="{{route('machining.start-progress.update',$machining->id)}}" style="display:none"
                            method="POST">
                            @csrf
                            <input name="start_progress" type="hidden" value="{{Carbon\Carbon::now()}}">
                            @method('PATCH')
                        </form>
                        <td>
                            <a href="#" onclick="
                            if(confirm('Kirim Info Start Progress?')){
                                event.preventDefault();document.getElementById('form-update-start-progress-{{$machining->id}}').submit();
                            }else{
                                event.preventDefault();
                            }
                                ">Start Progress</a>
                        </td>
                        @endif
                        <td>
                            {{$waktu_total}}
                        </td>

                        {{-- end progress --}}
                        @if($machining->end_progress != null)
                        <td nowrap="nowrap">
                            @php
                            $end = \Carbon\Carbon::parse($machining->end_progress)->format('d-M-y H:i:s');
                            @endphp
                            {{
                                substr($end,0,10)
                                }}
                            <br>
                            {{
                                substr($end,10)
                                }}
                        </td>
                        @else
                        <form id="form-update-end-progress-{{$machining->id}}"
                            action="{{route('machining.end-progress.update',$machining->id)}}" style="display:none"
                            method="POST">
                            @csrf
                            <input id="input-end-progress-{{$machining->id}}" name="end_progress" type="hidden"
                                value="">
                            @method('PATCH')
                        </form>
                        <td>
                            <a href="#" onclick="
                                if(confirm('Kirim Info Start Progress?')){
                                    event.preventDefault();document.getElementById('form-update-end-progress-{{$machining->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }
                                    ">End Progress</a>
                        </td>
                        @endif

                        {{-- realtime --}}
                        <td>
                            @if($machine_pause3 == null)
                            @if($machining->done == 0 && $machining->start_progress != null && $machining->end_progress ==null)
                            <div id="dataCountdown{{$machining->id}}"
                                data-countdown="{{$waktu_total}}|{{$machining->id}}|{{$total_pause}}">
                            </div>
                            @else
                            -
                            @endif
                            @else
                            delayed
                            @endif
                        </td>
                        <td>
                            <span id="status-time-{{$machining->id}}">Under</span>
                        </td>
                        <td>
                            <a href="{{route('machining.edit',$machining->id)}}" class="fa fa-edit fa-lg"
                                style="color:orange;text-decoration:none"></i> </a>
                            @if($machine_pause3 == null)
                                @if($machining->start_progress != null)
                                <a data-toggle="modal" data-target="#myModal{{$machining->id}}" href="#"> <i
                                        class="fa fa-pause fa-lg"></i></a>
                                @endif
                            @else
                                <form id="form-play-{{$machine_pause4->id}}"
                                    action="{{route('machining.unpause',$machine_pause4->id)}}" method="POST"
                                    style="display:none">
                                    @method('PATCH')
                                    @csrf
                                </form>
                                <a href="#"
                                    onclick="if(confirm('Delay Selesai?')){event.preventDefault();document.getElementById('form-play-{{$machine_pause4->id}}').submit();}else{event.preventDefault();}">
                                    <i class="fa fa-play fa-lg"></i></a>
                            @endif
                            <form id="form-delete-{{$machining->id}}"
                                action="{{route('machining.destroy',$machining->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                            <a href="#"
                                onclick="if(confirm('Yakin ingin menghapus?')){event.preventDefault();document.getElementById('form-delete-{{$machining->id}}').submit();}else{event.preventDefault();}"
                                class="fa fa-trash fa-lg" style="color:red;text-decoration:none">
                                </i>
                            </a>
                            @if($machining->start_progress != null)
                            <a href="{{route('machining.done',$machining->id)}}" class="fa fa-check fa-lg"
                                style="color:green;text-decoration:none"></i> </a>
                            @endif
                        </td>
                    </tr>

                    <div id="myModal{{$machining->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Pemberian Informasi Delay</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <form action="{{route('delay.store')}}" method="POST">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Masukan Alasan Delay :</label>
                                                <textarea name="status_pause" class="form-control"
                                                    placeholder="Alasan Delay" cols="30" rows="8"></textarea>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Pilih Helper Power :</label>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <select name="helper_power" class="form-control"
                                                            id="helper{{$machining->id}}" disabled>
                                                            <option value="" disabled selected>Pilih..</option>
                                                            <option value="Budi">Budi</option>
                                                            <option value="Samyang">Samyang</option>
                                                            <option value="Siapa">Siapa</option>
                                                            <option value="Adit">Adit</option>
                                                            <option disabled role=separator>
                                                            <option value="other{{$machining->id}}">Lainnya..</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="switch">
                                                            <input type="checkbox" id="helper_muncul{{$machining->id}}">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <br>
                                                <div id="input_helper{{$machining->id}}">
                                                    <label>Masukan Nama Helper</label>
                                                    <input type="text" class="form-control" placeholder="ex: Bambang"
                                                        name="helper_power_other">
                                                </div>
                                                <script>
                                                    var id = "{{$machining->id}}";
                                                    console.log(id);
                                                    $("#input_helper" + id).hide();
                                                    $("#helper_muncul" + id).change(function () {
                                                        if (this.checked) {
                                                            $("#helper{{$machining->id}}").removeAttr(
                                                                'disabled');
                                                        } else {
                                                            $("#helper{{$machining->id}}").attr('disabled',
                                                                'disabled');
                                                            $("#input_helper{{$machining->id}}").hide();
                                                        }
                                                    });
                                                    $("#helper{{$machining->id}}").change(function () {
                                                        if ($(this).val() == "other{{$machining->id}}") {
                                                            $("#input_helper{{$machining->id}}").show();
                                                        } else {
                                                            $("#input_helper{{$machining->id}}").hide();
                                                        }
                                                    });

                                                </script>
                                            </div>
                                        </div>
                                        {{-- <input type="text" name="status_pause" class="form-control" id="" placeholder="Alasan Delay"> --}}
                                        <input type="hidden" name="machine_id" value="{{$machining->id}}">
                                        <input type="hidden" name="man_power" value="{{$machining->man_power}}">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Kirim!</button>
                                        <button type="button" class="btn btn-default"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    @endif
                    @endforeach
                    @endforeach
                </tbody>

                {{-- machine stop --}}
            </table>
        </div> <!-- /.table-stats -->
        <br>
        Machine Stop
        <div class="table-stats order-table ov-h">
            <table class="" style="text-align:center">
                <thead>
                    <tr>
                        <th>nama mesin</th>
                        <th>Status Mesin</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($machinings as $machining)
                    @if($machining->kondisi_mesin != 'runing')
                    <tr>
                        <td>{{$machining->nama_mesin}}</td>
                        <td>{{$machining->kondisi_mesin}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    $('[data-countdown]').each(function () {
        var $this = $(this),
            finalDate = $(this).data('countdown'),
            array_waktu = finalDate.split('|'),
            waktu_akhir = new Date(array_waktu[0]),
            machining_id = array_waktu[1],
            total_delay = parseInt(array_waktu[2]);
        var timestamp = (waktu_akhir - Date.now());
        timestamp /= 1000;
        timestamp = timestamp + total_delay;

        function component(x, v) {
            return Math.floor(x / v);
        }
        setInterval(function () {

            timestamp--;

            var days = component(timestamp, 24 * 60 * 60),
                hours = component(timestamp, 60 * 60) % 24,
                minutes = component(timestamp, 60) % 60,
                seconds = component(timestamp, 1) % 60;

            if (timestamp > 0) {
                if (days < 0) {
                    $this.html(hours + ":" + minutes + ":" + seconds);
                } else
                    $this.html(days + " days, " + hours + ":" + minutes + ":" + seconds);
            } else {
                $this.html(days + " days, " + hours + ":" + minutes + ":" + seconds);
                $('#status-time-' + machining_id).html('Over');

            }

        }, 1000);


    });

</script>

@endsection
