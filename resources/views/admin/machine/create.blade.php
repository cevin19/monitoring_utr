@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Add New Machine</strong>
        <a class="btn btn-warning float-right" href="{{route('machining.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        <form action="{{route('machining.new-machine')}}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Register:</label>
                            <input type="text" name="register" class="form-control" placeholder="ex: BPC190001">
                        </div>
                        <div class="col-md-6">
                            <label>PRO Number</label>
                            <input type="text" name="pro_number" class="form-control" placeholder="ex: 61000001">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Unit Model</label>
                            <select name="unit_model" id="unit_model" class="form-control">
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $model_units = $routings->unique('model_unit'); $model_units->values()->all();
                                @endphp
                                @foreach($model_units as $model_unit)
                                <option value="{{$model_unit->model_unit}}">{{$model_unit->model_unit}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Komponen</label>
                            <select name="komponen" id="komponen" class="form-control" disabled>
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $komponens = $routings->unique('komponen'); $komponens->values()->all();
                                @endphp
                                @foreach($komponens as $komponen)
                                <option value="{{$komponen->komponen}}" class="{{$komponen->model_unit}}">
                                    {{$komponen->komponen}}</option>
                                {{-- <option value="Boom" class="pc1000">Boom</option>
                                    <option value="BUC" class="pc2000">Bucket</option> --}}
                                @endforeach
                            </select>
                            <script>
                                $("#unit_model").change(function(){
                                        $('#komponen').removeAttr('disabled');
                                        if($(this).val()=="PC2000"){    
                                            $(".HD785-7").hide();
                                            $(".PC2000").show();
                                        }
                                        else if($(this).val()=="PC1250"){    
                                            $(".HD785-7").hide();
                                            $(".PC2000").show();
                                        }
                                        else if($(this).val()=="PC750/PC800"){    
                                            $(".HD785-7").hide();
                                            $(".PC2000").show();
                                        }
                                        else{
                                            $(".PC2000").hide();
                                            $(".HD785-7").show();
                                        }
                                    });
                            </script>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Sub Komponen</label>
                            <select name="sub_komponen" id="sub_komponen" class="form-control" disabled>
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $sub_komponens = $routings->unique('sub_komponen'); $sub_komponens->values()->all();
                                @endphp
                                @foreach($sub_komponens as $sub_komponen)
                                <option value="{{$sub_komponen->sub_komponen}}" class="sub_{{$komponen->model_unit}}">
                                    {{$sub_komponen->sub_komponen}}</option>
                                {{-- <option value="Boom" class="pc1000">Boom</option>
                                    <option value="BUC" class="pc2000">Bucket</option> --}}
                                @endforeach
                            </select>
                            <script>
                                $("#komponen").change(function(){
                                    $('#sub_komponen').removeAttr('disabled');
                                    if($(this).val()=="Rod"){    
                                        $(".HD785-7").hide();
                                        $(".PC2000").show();
                                    }
                                });
                            </script>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Tipe Kerusakan</label>
                            <select name="kategori_kerusakan" id="tipe_kerusakan" class="form-control" disabled>
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $tipe_kerusakans = $routings->unique('kategori_kerusakan');
                                $tipe_kerusakans->values()->all();
                                @endphp
                                @foreach($tipe_kerusakans as $tipe_kerusakan)
                                <option value="{{$tipe_kerusakan->kategori_kerusakan}}">
                                    {{$tipe_kerusakan->kategori_kerusakan}}</option>
                                @endforeach
                            </select>
                            <script>
                                var komponen = $("#komponen").val();
                                var subKomponen = $("#sub_komponen").val();
                                $("#sub_komponen").change(function(){
                                    $('#tipe_kerusakan').removeAttr('disabled');
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Progress Job</label>
                            <select name="progress_job" class="form-control" id="progress_job" disabled>
                                <option value="" selected disabled>Pilih</option>
                                @php
                                $activities = $routings->unique('activity'); $activities->values()->all();
                                @endphp
                                @foreach ($activities as $activity)
                                <option value="{{$activity->activity}}" class="activity_job"
                                    sub-komponen="{{$activity->sub_komponen}}"
                                    kategori-kerusakan="{{$activity->kategori_kerusakan}}">{{$activity->activity}}
                                </option>
                                @endforeach
                            </select>
                            <script>
                                $("#sub_komponen").change(function(){
                                    $('#progress_job').attr('disabled','disabled');
                                });
                                $("#tipe_kerusakan").change(function(){
                                    var sub_komponen = $('#sub_komponen').val();
                                    var kerusakan = $('#tipe_kerusakan').val();
                                    $('#progress_job').removeAttr('disabled');
                                    $('.activity_job').hide();
                                    $('.activity_job[sub-komponen*='+ sub_komponen +'][kategori-kerusakan*='+ kerusakan +']').show();
                                });
                            </script>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Start Progress</label>
                            <input type="datetime-local" name="start_progress" class="form-control"
                                placeholder="Start Progress">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Man Power</label>
                            <select class="form-control" name="man_power">
                                @foreach($man_powers as $man_power)
                                <option value="{{$man_power->id}}">{{$man_power->nama}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nama Mesin</label>
                            <select class="form-control" name="machine_id">
                                @foreach($machines as $machine)
                                <option value="{{$machine->id}}" @if($machine->kondisi_mesin == 'runing') disabled
                                    style="color:orange" @elseif($machine->kondisi_mesin=='breakdown') disabled
                                    style="color:red" @else style="color:green" @endif>{{$machine->nama_mesin}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-success form-control col-md-10">Tambah</button>
        </form>

    </div>
</div>
@endsection