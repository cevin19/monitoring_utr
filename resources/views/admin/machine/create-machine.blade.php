@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Edit Machine</strong>
        <a class="btn btn-warning float-right" href="{{route('machining.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        
            <div class="row mx-auto">

                <table class="table  col-md-6" id="dynamic_field">
                    <thead>
                        <tr>
                            
                            <th scope="col">Nama Mesin</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody class="container1">
                        
                        @foreach($machines as $machine)
                            @if($machine->done == 0)
                                <tr>
                                    
                                    <td><span>{{$machine->nama_mesin}}</span></td>
                                    <td><span>{{$machine->kondisi_mesin}}</span></td>
                                    <form id="form-delete-{{$machine->id}}" action="{{route('machining.destroy',$machine->id)}}" style="display:none" method="POST">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <td>
                                       @php
                                       $kondisi = $machine->kondisi_mesin;
                                       @endphp
                                        @if($kondisi != 'ready')
                                        <a href="#" onclick="alert('tidak dapat menghapus karna mesin dalam status runing!')"><i class="fa fa-trash fa-lg" style="color:red"></i></a>
                                        @else
                                        <a href="#" onclick="
                                        if(confirm('Yakin ingin menghapus?')){
                                            event.preventDefault();document.getElementById('form-delete-{{$machine->id}}').submit();
                                        }else{
                                            event.preventDefault();
                                        }
                                            " class="fa fa-trash fa-lg" style="color:red;"></i> </a>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                        @endforeach
                        
                        <form action="{{route('machine.store')}}" method="POST">
                            @csrf 
                            <tr>
                                <td>#</td>
                                <td>
                                    <input type="text" name="nama_mesin" >
                                </td>
                                <td><button type="submit">tambah</button></td>
                            </tr>
                        </form>
                    </tbody>
                </table>
            </div>


    </div>
</div>
<script>
    $(document).ready(function () {
        var max_fields = 10;
        var wrapper = $(".container1");
        var add_button = $(".add_form_field");
        var x = 1;
        $(add_button).click(function (e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append(
                    '<tr id="row'+x+'"><td> ' + x + ' </td><td><input class="form-control" name="routings[]" id="answer" list="data-routing"><input name="urutan[]"  type="hidden" value="'+ x +'"></td><td><a href="#" class="delete btn btn-danger">Delete</a></td></tr>');
            } else {
                alert('You Reached the limits')
            }
        });

        $(wrapper).on("click", ".delete", function (e) {
            e.preventDefault();
            $('#row'+x+'').remove();
            x--;
        })
    });

</script>
@endsection