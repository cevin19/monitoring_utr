@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Routing Table</strong>
        <a class="btn btn-danger float-right" href="{{route('routing.create')}}">Create New</a>
    </div>

    <div class="card-body">

        <div class="table-stats order-table ov-h">
            <table class="table ">
                <thead>
                    <tr>
                        <th class="serial" style="padding-top:14px;">#</th>
                        <th>Grup</th>
                        <th>Model Unit</th>
                        <th>Komponen</th>
                        <th>Sub Komponen</th>
                        <th>Kat Kerusakan</th>
                        <th>Activity</th>
                        <th>Estimasi LT Prod.</th>
                        <th>Aksi</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($routings as $routing)

                    <tr>
                        <td class="serial">{{$loop->index + 1}}</td>
                        <td>{{$routing->grup}}</td>
                        <td>{{$routing->model_unit}}</td>
                        <td>{{$routing->komponen}}</td>
                        <td>{{$routing->sub_komponen}}</td>
                        <td>{{$routing->kategori_kerusakan}}</td>
                        <td>{{$routing->activity}}</td>
                        <td>{{$routing->estimasi}}</td>

                        <td>
                            <a href="{{route('routing.edit',$routing->id)}}" class="fa fa-edit fa-lg" style="color:orange;"></i> </a>

                            <form id="form-delete-{{$routing->id}}" action="{{route('routing.destroy',$routing->id)}}" style="display:none" method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                            <a href="#" onclick="
                                if(confirm('Yakin ingin menghapus?')){
                                    event.preventDefault();document.getElementById('form-delete-{{$routing->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }
                                    " class="fa fa-trash fa-lg" style="color:red;"></i> </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div> <!-- /.table-stats -->
    </div>
</div>
@endsection