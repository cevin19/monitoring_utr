@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Add New Routing</strong>
        <a class="btn btn-warning float-right" href="{{route('routing.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        <hr>
        <form action="{{route('routing.store')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Grup</label>
                            <input type="text" name="grup" class="form-control" placeholder="ex: CYL">
                        </div>
                        <div class="col-md-6">
                            <label>Model Unit</label>
                            <input type="text" name="model_unit" class="form-control" placeholder="ex: PC2000">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Komponen</label>
                            <input type="text" name="komponen" class="form-control" placeholder="ex: Boom">
                        </div>
                        <div class="col-md-6">
                            <label>Sub Komponen</label>
                            <input type="text" name="sub_komponen" class="form-control" placeholder="ex: Gland">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Kategori Kerusakan</label>
                            <input type="text" name="kategori_kerusakan" class="form-control" placeholder="ex: Ringan">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Activity</label>
                            <textarea name="activity" rows="5" placeholder="ex: Polish Gland" class="form-control"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Estimasi</label>
                            <input type="number" name="estimasi" class="form-control" placeholder="ex: 4">
                            <label class="text-danger">*hanya isi angka dalam hitungan jam</label>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success form-control col-md-10">Tambah</button>
        </form>
        <br>
        <hr>
        <br>
        <form action="{{route('routing.excel')}}" method="POST" enctype="multipart/form-data">
            <h4>Tambahkan dengan excel</h4>
            @csrf
            <input type="file" name="file" id="">
            <button type="submit" class="btn btn-success">Tambah dari excel</button>
        </form>

    </div>
</div>
@endsection