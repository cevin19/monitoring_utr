@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Edit Machine Data</strong>
            <a class="btn btn-warning float-right" href="{{route('machine.index')}}">Back</a>
        </div>

        <div class="card-body">
            <form action="{{route('machine.update',$machine->id)}}" method="POST">
                @csrf @method('PATCH')
                <input type="text" name="name" class="form-control" placeholder="Machine Name" value="{{$machine->name}}" >
                <input type="text" name="damage_type" class="form-control" placeholder="Jenis Kerusakan" value="{{$machine->damage_type}}" >
                <input type="date" name="date_in" class="form-control" placeholder="Tanggal Masuk" value="{{$machine->date_in}}" >
                <a href="#" id="button-select-plant" onclick="
                document.getElementById('button-select-plant').style.display=('none');
                document.getElementById('select-plant').style.display=('block');
                ">pilih plant</a>
                <select name="plant_id" id="select-plant" style="display:none">
                    @foreach ($plants as $plant)    
                    <option value="{{$plant->id}}">{{$plant->name}}</option>
                    @endforeach
    
                </select>
                <button type="submit" class="btn btn-success">Update</button>
                <a href="{{route('machine.index')}}" class="btn btn-warning">Kembali</a>
            </form>
        </div>
    </div>
@endsection