@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Machine Show</strong>
            <a class="btn btn-warning float-right" href="{{route('plant.index')}}">Back</a>
        </div>

        <div class="card-body">

            <input type="text" name="machine" class="form-control" placeholder="Machine Name" value="{{$machine->name}}" readonly>
            <input type="text" name="tipe_kerusakan" class="form-control" placeholder="Jenis Kerusakan" value="{{$machine->damage_type}}" readonly>
            <input type="date" name="tanggal_masuk" class="form-control" placeholder="Tanggal Masuk" value="{{$machine->date_in}}" readonly>

            <a href="{{route('machine.index')}}" class="btn btn-warning">Kembali</a>
        </div>
    </div>
@endsection