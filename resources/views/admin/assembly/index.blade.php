@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Index Assembly</strong>
        @if(Auth::user()->role == 'admin')
        <a class="btn btn-danger float-right " style="margin-left:2px" href="{{route('assembly.create')}}">Tambah
            Assembly</a>
        @endif
    </div>

    <div class="card-body">
        <div class="table-stats ov-h">
            <table class="" style="text-align:center" id="assembly" class="display">
                <thead>
                    <tr style="background-color:#e8e9ef">
                        <th>#</th>
                        <th>register</th>
                        <th>PRO Number</th>
                        <th>unit model</th>
                        <th>nama komponen</th>
                        <th>tube</th>
                        <th>rod</th>
                        <th>piston</th>
                        <th>gland</th>
                        <th>midle</th>
                        <th>seal kit</th>
                        <th>bushing</th>
                        <th>part related</th>
                        <th>status komp</th>
                        <th>estimasi rfu</th>
                        <th>skala prioritas</th>
                        <th>estimasi assembly</th>
                        <th>delay time</th>
                        <th>progress</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($assemblies as $assembly)
                    <tr>
                        <td class="serial">{{$loop->index +1}}</td>
                        <td>{{$assembly->register}}</td>
                        <td>{{$assembly->pro_number}}</td>
                        <td>{{$assembly->unit_model}}</td>
                        <td>{{$assembly->nama_komponen}}</td>
                        <td>
                            @if($assembly->tube == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <form id="form-update-tube-{{$assembly->id}}"
                                action="{{route('assembly.update.tube',$assembly->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                <input type="hidden" name="tube" id="" value="OK">
                                @method('PATCH')
                            </form>
                            <a href="#" class="text-danger" onclick="
                                if(confirm('Apakah Sudah Oke?')){
                                    event.preventDefault();document.getElementById('form-update-tube-{{$assembly->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }"><i class="text-danger fa fa-times"></i></a>
                            @endif
                        </td>
                        <td>
                            @if($assembly->rod == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <form id="form-update-rod-{{$assembly->id}}"
                                action="{{route('assembly.update.rod',$assembly->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                <input type="hidden" name="rod" id="" value="OK">
                                @method('PATCH')
                            </form>
                            <a href="#" class="text-danger" onclick="
                                if(confirm('Apakah Sudah Oke?')){
                                    event.preventDefault();document.getElementById('form-update-rod-{{$assembly->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }"><i class="text-danger fa fa-times"></i></a>
                            @endif
                        </td>
                        <td>
                            @if($assembly->piston == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <form id="form-update-piston-{{$assembly->id}}"
                                action="{{route('assembly.update.piston',$assembly->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                <input type="hidden" name="piston" id="" value="OK">
                                @method('PATCH')
                            </form>
                            <a href="#" class="text-danger" onclick="
                                if(confirm('Apakah Sudah Oke?')){
                                    event.preventDefault();document.getElementById('form-update-piston-{{$assembly->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }"><i class="text-danger fa fa-times"></i></a>
                            @endif
                        </td>

                        <td>
                            @if($assembly->gland == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <form id="form-update-gland-{{$assembly->id}}"
                                action="{{route('assembly.update.gland',$assembly->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                <input type="hidden" name="gland" id="" value="OK">
                                @method('PATCH')
                            </form>
                            <a href="#" class="text-danger" onclick="
                                if(confirm('Apakah Sudah Oke?')){
                                    event.preventDefault();document.getElementById('form-update-gland-{{$assembly->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }"><i class="text-danger fa fa-times"></i></a>
                            @endif
                        </td>
                        <td>
                            @if($assembly->midle == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <form id="form-update-midle-{{$assembly->id}}"
                                action="{{route('assembly.update.midle',$assembly->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                <input type="hidden" name="midle" id="" value="OK">
                                @method('PATCH')
                            </form>
                            <a href="#" class="text-danger" onclick="
                                if(confirm('Apakah Sudah Oke?')){
                                    event.preventDefault();document.getElementById('form-update-midle-{{$assembly->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }"><i class="text-danger fa fa-times"></i></a>
                            @endif
                        </td>
                        <td>
                            @if($assembly->seal_kit == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <form id="form-update-seal-kit-{{$assembly->id}}"
                                action="{{route('assembly.update.seal_kit',$assembly->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                <input type="hidden" name="seal_kit" id="" value="OK">
                                @method('PATCH')
                            </form>
                            <a href="#" class="text-danger" onclick="
                                if(confirm('Apakah Sudah Oke?')){
                                    event.preventDefault();document.getElementById('form-update-seal-kit-{{$assembly->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }"><i class="text-danger fa fa-times"></i></a>
                            @endif
                        </td>
                        <td>
                            @if($assembly->bushing == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <form id="form-update-bushing-{{$assembly->id}}"
                                action="{{route('assembly.update.bushing',$assembly->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                <input type="hidden" name="bushing" id="" value="OK">
                                @method('PATCH')
                            </form>
                            <a href="#" class="text-danger" onclick="
                                if(confirm('Apakah Sudah Oke?')){
                                    event.preventDefault();document.getElementById('form-update-bushing-{{$assembly->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }"><i class="text-danger fa fa-times"></i></a>
                            @endif
                        </td>
                        <td>
                            @if($assembly->part_related == "Full")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <form id="form-update-part-related-{{$assembly->id}}"
                                action="{{route('assembly.update.part_related',$assembly->id)}}" style="display:none"
                                method="POST">
                                @csrf
                                <input type="hidden" name="part_related" id="" value="Full">
                                @method('PATCH')
                            </form>
                            <a href="#" class="text-danger" onclick="
                                if(confirm('Apakah Sudah Oke?')){
                                    event.preventDefault();document.getElementById('form-update-part-related-{{$assembly->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }"><i class="text-danger fa fa-times"></i></a>
                            @endif
                        </td>
                        <td>{{$assembly->status_komp}}</td>
                        <td>{{$assembly->estimasi_rfu}}</td>
                        <td>{{$assembly->skala_prioritas}}</td>
                        <td>{{$assembly->estimasi_assembly}}</td>
                        <td>{{$assembly->delay_time}}</td>
                        @php
                        $banyak_data = 1;
                        if($assembly->tube == "OK"){
                        $banyak_data++;
                        }
                        if($assembly->rod == "OK"){
                        $banyak_data++;
                        }
                        if($assembly->piston == "OK"){
                        $banyak_data++;
                        }
                        if($assembly->gland == "OK"){
                        $banyak_data++;
                        }
                        if($assembly->midle == "OK"){
                        $banyak_data++;
                        }
                        if($assembly->seal_kit == "OK"){
                        $banyak_data++;
                        }
                        if($assembly->bushing == "OK"){
                        $banyak_data++;
                        }
                        if($assembly->part_related == "OK"){
                        $banyak_data++;
                        }
                        $persentasi=substr(($banyak_data/8)*100,0,4);
                        @endphp
                        @if ($persentasi <= 45) <td>
                            <div class="progress">
                                <div class="progress-bar bg-danger" role="progressbar"
                                    style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}"
                                    aria-valuemin="0" aria-valuemax="100">{{$persentasi}}%</div>
                            </div>
                            </td>
                            @elseif ($persentasi <= 99) <td>
                                <div class="progress">
                                    <div class="progress-bar bg-warning" role="progressbar"
                                        style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}"
                                        aria-valuemin="0" aria-valuemax="100">{{$persentasi}}%</div>
                                </div>
                                </td>
                                @elseif ($persentasi == 100)
                                <td>
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar"
                                            style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}"
                                            aria-valuemin="0" aria-valuemax="100">Done!</div>
                                    </div>
                                </td>
                                @endif
                                <td>
                                    <a href="{{route('assembly.edit',$assembly->id)}}" class="fa fa-edit fa-lg"
                                        style="color:orange;"></i>
                                    </a>
                                    <form id="form-delete-{{$assembly->id}}"
                                        action="{{route('assembly.destroy',$assembly->id)}}" style="display:none"
                                        method="POST">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <a href="#"
                                        onclick="if(confirm('Yakin ingin menghapus?')){event.preventDefault();document.getElementById('form-delete-{{$assembly->id}}').submit();}else{event.preventDefault();}"
                                        class="fa fa-trash fa-lg" style="color:red;">
                                        </i>
                                    </a>

                                </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <br>

        </div> <!-- /.table-stats -->
    </div>
</div>

@endsection