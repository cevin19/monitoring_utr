@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <strong class="card-title">PDC Show</strong>
            <a class="btn btn-warning float-right" href="{{route('PDC.index')}}">Back</a>
        </div>

        <div class="card-body">
                    <div class="row mx-auto">
        
                        <table class="table  col-md-6" id="dynamic_field">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Urutan Job</th>
                                    <th scope="col">Routing</th>
                                </tr>
                            </thead>
                            <tbody class="container1">
                                @foreach($pdc->routings as $pdcrouting)
                                <tr>
                                    <td >{{$loop->index + 1}}</td>
                                    <td class="id-routing">{{$pdcrouting->activity}}</td>
                                    <td><span class="estimasi-routing" id="estimasi-id-{{$loop->index + 1}}">{{$pdcrouting->estimasi}}</span></td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
           
                    <div class="table-stats order-table ov-h">
                            <table class="table ">
                                <thead>
                                    <tr>
                                        <th class="serial">#</th>
                                        <th>register</th>
                                        <th>unit model</th>
                                        <th>nama komponen</th>
                                        <th>date in</th>
                                        <th>progress job</th>
                                        <th>routing</th>
                                        <th>start progress</th>
                                        <th>date out</th>
                                        <th>lead time</th>
                                        <th>std leadtime</th>
                                        <th>status leadtime</th>
                                        <th>plan ready go ahead</th>
                                        <TH>Real Time</TH>
                                        <th>%tage progress</th>
            
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                        
                                    <tr>
                                        <td class="serial">{{$pdc->id}}</td>
                                        <td>{{$pdc->register}}</td>
                                        <td>{{$pdc->unit_model}}</td>
                                        <td>{{$pdc->nama_komponen}}</td>
                                        <td>{{$pdc->date_in}}</td>
                                        <td>{{$pdc->progress_job}}</td>
                                        <td id="sisa"></td>
                                        <td>{{$pdc->start_progress}}</td>
                                        <td>{{$pdc->date_out}}</td>
                                        <td>{{$pdc->leadtime}}</td>
                                        <td>{{$pdc->std_leadtime_id}}</td>
                                        <td>{{$pdc->status_leadtime}}</td>
                                        <td>{{$pdc->plan_ready_go_ahead}}</td>
                                        <td id="the-final-countdown"></td>
                                        <td  style="display:none" class="routing-tampil"> {{$pdc->progress_job}}</td>
                                        <td>{{$pdc->percentage_progress}}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div> <!-- /.table-stats -->
            </div>
    </div>
    
    <script>
        $( document ).ready(function() {
            var elemen = document.getElementsByClassName('id-routing');
            var elemen_tampil = document.getElementsByClassName('routing-tampil');



            var arr = jQuery.makeArray(elemen);
            var arr_tampil = jQuery.makeArray(elemen_tampil);
            var arrL = arr.length;
            var arr_tampilL = arr_tampil.length;
                
            for(i=0;i<arrL;i++){
                routing = $(arr[i]).text();
                for(x=0;x<arr_tampilL;x++){
                    hasilRouting= $(arr_tampil[i]).text();
                    if(routing=hasilRouting){
                        a= x+1;
                        datatampil= document.getElementById('estimasi-id-' + a);
                        // console.log($(datatampil).text());
                    }
                }
                
            }

            // $(arr).each(function() {
            //     console.log(Object.keys(arr));
            //     var arrData = $(this).text();
            //     $(arr_tampil).each(function() {
            //         arrTampil= $(this).text();
            //         if(arrTampil = arrData){
                        
            //             // var hasilAkhir = document.getElementById('estimasi-id-').text();
            //             // console.log(Object.keys(arrTampil));
            //         }
            //     });
            // });
        });

        var waktu = 4*60*60;
        
            setInterval(function time(){
                var d = new Date();
                var hours = 24 - d.getHours();
                var min = 60 - d.getMinutes();
                if((min + '').length == 1){
                    min = '0' + min;
                }
                var sec = 60 - d.getSeconds();
                if((sec + '').length == 1){
                        sec = '0' + sec;
                }
                jQuery('#the-final-countdown').html(hours+':'+min+':'+sec)
                }, 1000);
    </script>
    
@endsection
