@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Edit Routing PDC</strong>
        <a class="btn btn-warning float-right" href="{{route('PDC.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        
            <div class="row mx-auto">

                <table class="table  col-md-6" id="dynamic_field">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Urutan Job</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="container1">
                        
                        @foreach($routings as $routing)
                            @foreach($relation_routings as $relation_routing)
                                @if($routing->id == $relation_routing->routing_id)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td><span>{{$routing->activity}}</span></td>
                                        <form id="form-delete-{{$relation_routing->id}}" action="{{route('relation_routing.destroy.pdc',$relation_routing->id)}}" style="display:none" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <td><a href="#" onclick="
                                            if(confirm('Yakin ingin menghapus?')){
                                                event.preventDefault();document.getElementById('form-delete-{{$relation_routing->id}}').submit();
                                            }else{
                                                event.preventDefault();
                                            }
                                                " class="fa fa-trash fa-lg" style="color:red;"></i> </a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                        
                        <form action="{{route('relation_routing.store.pdc')}}" method="POST">
                            @csrf 
                            <tr>
                                <td>#</td>
                                <td>
                                    <input type="text" name="routing_pdc" list="data-routing">
                                </td>
                                <td><button type="submit">tambah</button></td>
                            </tr>
                        </form>
                    </tbody>
                </table>
            </div>


    </div>
</div>
<datalist id="data-routing">
    @foreach($routings as $routing)
    <option value="{{$routing->id}}">{{$routing->activity}}</option>
    @endforeach
</datalist>
<script>
    $(document).ready(function () {
        var max_fields = 10;
        var wrapper = $(".container1");
        var add_button = $(".add_form_field");
        var x = 1;
        $(add_button).click(function (e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append(
                    '<tr id="row'+x+'"><td> ' + x + ' </td><td><input class="form-control" name="routings[]" id="answer" list="data-routing"><input name="urutan[]"  type="hidden" value="'+ x +'"></td><td><a href="#" class="delete btn btn-danger">Delete</a></td></tr>');
            } else {
                alert('You Reached the limits')
            }
        });

        $(wrapper).on("click", ".delete", function (e) {
            e.preventDefault();
            $('#row'+x+'').remove();
            x--;
        })
    });

</script>
@endsection
