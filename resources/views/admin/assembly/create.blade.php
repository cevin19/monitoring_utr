@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Add New Assembly</strong>
        <a class="btn btn-warning float-right" href="{{route('assembly.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        <form action="{{route('assembly.store')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Register:</label>
                            <input type="text" name="register" class="form-control" placeholder="ex: BPC190001">
                        </div>
                        <div class="col-md-6">
                            <label>PRO Number</label>
                            <input type="text" name="pro_number" class="form-control" placeholder="ex: 61000001">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Unit Model</label>
                            <select name="model_unit" id="unit_model" class="form-control">
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $model_units = $routings->unique('model_unit'); $model_units->values()->all();
                                @endphp
                                @foreach($model_units as $model_unit)
                                <option value="{{$model_unit->model_unit}}">{{$model_unit->model_unit}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Nama Komponen</label>
                            <select name="nama_komponen" id="komponen" class="form-control" disabled>
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $komponens = $routings->unique('komponen'); $komponens->values()->all();
                                @endphp
                                @foreach($komponens as $komponen)
                                <option value="{{$komponen->komponen}}" class="{{$komponen->model_unit}}">
                                    {{$komponen->komponen}}</option>
                                {{-- <option value="Boom" class="pc1000">Boom</option>
                                        <option value="BUC" class="pc2000">Bucket</option> --}}
                                @endforeach
                            </select>
                            <script>
                                $("#unit_model").change(function(){
                                            $('#komponen').removeAttr('disabled');
                                            if($(this).val()=="PC2000"){    
                                                $(".HD785-7").hide();
                                                $(".PC2000").show();
                                            }
                                            else if($(this).val()=="PC1250"){    
                                                $(".HD785-7").hide();
                                                $(".PC2000").hide();
                                            }
                                            else if($(this).val()=="PC750/PC800"){    
                                                $(".HD785-7").hide();
                                                $(".PC2000").hide();
                                            }
                                            else{
                                                $(".PC2000").hide();
                                                $(".HD785-7").show();
                                            }
                                        });
                            </script>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Status Komp.:</label>
                            <select name="status_komp" class="form-control">
                                <option value="" disabled selected>Pilih..</option>
                                <option value="Plan">Plan</option>
                                <option value="Plan">Unplan</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Estimasi RFU:</label>
                            <input type="text" name="estimasi_rfu" class="form-control" placeholder="ex: W1">
                        </div>
                        <div class="col-md-8">
                            <label>Skala Prioritas</label>
                            <input type="text" name="skala_prioritas" class="form-control"
                                placeholder="Skala Prioritas">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Estimasi Assembly:</label>
                            <input type="text" name="estimasi_assembly" class="form-control"
                                placeholder="Estimasi Assembly">
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <label style="margin-left:170px;">Status Pengerjaan</label>
                            <br><br>
                            <div class="table-stats order-table ov-h" style="margin-left:50px;">
                                <table style="text-align:center; width:100%;">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Sub Komponen</th>
                                            <th colspan="2" style="padding-right:70px">Status</th>
                                        </tr>
                                        <tr>
                                            <th style="padding-left:50px">Ok</th>
                                            <th style="padding-right:40px">Not</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Tube</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="tube" value="OK">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="tube" value="Not" checked>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rod</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="rod" value="OK">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="rod" value="Not" checked>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Piston</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="piston" value="OK">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="piston" value="Not" checked>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gland</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="gland" value="OK">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="gland" value="Not" checked>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Midle</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="midle" value="OK">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="midle" value="Not" checked>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Seal Kit</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="seal_kit" value="OK">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="seal_kit" value="Not" checked>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bushing</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="bushing" value="OK">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="bushing" value="Not" checked>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Part Related</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="part_related" value="Full">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="part_related" value="Not Full" checked>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <label class="text-danger">*Select pada lingkaran untuk menambahkan status</label>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-success form-control col-md-10">Tambah</button>
        </form>

    </div>
</div>
@endsection