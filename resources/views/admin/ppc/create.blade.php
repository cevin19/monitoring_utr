@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Add New PPC</strong>
        <a class="btn btn-warning float-right" href="{{route('PPC.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        <form action="{{route('PPC.store')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Register:</label>
                            <input type="text" name="register" class="form-control" placeholder="ex: BPC190001">
                        </div>
                        <div class="col-md-6">
                            <label>PRO Number</label>
                            <input type="text" name="pro_number" class="form-control" placeholder="ex: 61000001">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Unit Model</label>
                            <input type="text" name="unit_model" class="form-control" placeholder="ex: PC2000">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nama Komponen</label>
                            <input type="text" name="nama_komponen" class="form-control" placeholder="ex: BUC">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Progress Job</label>
                            <select name="progress_job" class="form-control">
                                <option value="" selected disabled>Pilih</option>
                                @foreach($relation_routings as $relation_routing)
                                @foreach($routings as $routing)
                                @if($relation_routing->routing_id== $routing->id )
                                <option value="{{$routing->id}}">{{$routing->activity}}</option>
                                @endif
                                @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Date In</label>
                            <input type="date" name="date_in" class="form-control" placeholder="Date In">
                        </div>
                        <div class="col-md-6">
                            <label>Date Out</label>
                            <input type="date" name="date_out" class="form-control" placeholder="Date Out">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Start Progress</label>
                            <input type="datetime-local" name="start_progress" class="form-control"
                                placeholder="Start Progress">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Man Power</label>
                            <select name="man_powers[]" multiple id="" class="form-control">
                                @foreach($man_powers as $man_power)
                                <option value="{{$man_power->id}}">{{$loop->index + 1}}. {{$man_power->nama}}</option>
                                @endforeach
                            </select>
                            <label class="text-danger">*Man Power dapat lebih dari 1 dengan ctrl + klik</label>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-success form-control col-md-10">Tambah</button>
        </form>

    </div>
</div>
<datalist id="data-routing">
    @foreach($routings as $routing)
    <option value="{{$routing->id}}">{{$routing->activity}}</option>
    @endforeach
</datalist>
@endsection