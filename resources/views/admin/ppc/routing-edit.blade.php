@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Edit Routing PPC</strong>
        <a class="btn btn-warning float-right" href="{{route('PPC.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')

        <div class="row mx-auto">
            <div class="col-md-5">
                <form action="{{route('relation_routing.store.ppc')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <label>Unit Model</label>
                            <select name="unit_model" id="unit_model" class="form-control">
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $model_units = $routings->unique('model_unit'); $model_units->values()->all();
                                @endphp
                                @foreach($model_units as $model_unit)
                                <option value="{{$model_unit->model_unit}}">{{$model_unit->model_unit}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Komponen</label>
                            <select name="komponen" id="komponen" class="form-control" disabled>
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $komponens = $routings->unique('komponen'); $komponens->values()->all();
                                @endphp
                                @foreach($komponens as $komponen)
                                <option value="{{$komponen->komponen}}" class="{{$komponen->model_unit}}">
                                    {{$komponen->komponen}}</option>
                                {{-- <option value="Boom" class="pc1000">Boom</option>
                                                        <option value="BUC" class="pc2000">Bucket</option> --}}
                                @endforeach
                            </select>
                            <script>
                                $("#unit_model").change(function () {
                                                $('#komponen').removeAttr('disabled');
                                                if ($(this).val() == "PC2000") {
                                                    $(".HD785-7").hide();
                                                    $(".PC2000").show();
                                                } else if ($(this).val() == "PC1250") {
                                                    $(".HD785-7").hide();
                                                    $(".PC2000").show();
                                                } else if ($(this).val() == "PC750/PC800") {
                                                    $(".HD785-7").hide();
                                                    $(".PC2000").show();
                                                } else {
                                                    $(".PC2000").hide();
                                                    $(".HD785-7").show();
                                                }
                                            });
                        
                            </script>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Sub Komponen</label>
                            <select name="sub_komponen" id="sub_komponen" class="form-control" disabled>
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $sub_komponens = $routings->unique('sub_komponen'); $sub_komponens->values()->all();
                                @endphp
                                @foreach($sub_komponens as $sub_komponen)
                                <option value="{{$sub_komponen->sub_komponen}}" class="sub_{{$komponen->model_unit}}">
                                    {{$sub_komponen->sub_komponen}}</option>
                                {{-- <option value="Boom" class="pc1000">Boom</option>
                                                        <option value="BUC" class="pc2000">Bucket</option> --}}
                                @endforeach
                            </select>
                            <script>
                                $("#komponen").change(function () {
                                                $('#sub_komponen').removeAttr('disabled');
                                                if ($(this).val() == "Rod") {
                                                    $(".HD785-7").hide();
                                                    $(".PC2000").show();
                                                }
                                            });
                        
                            </script>
                        </div>
                        <div class="col-md-6">
                            <label>Tipe Kerusakan</label>
                            <select name="kategori_kerusakan" id="tipe_kerusakan" class="form-control" disabled>
                                <option value="" disabled selected>Pilih..</option>
                                @php
                                $tipe_kerusakans = $routings->unique('kategori_kerusakan');
                                $tipe_kerusakans->values()->all();
                                @endphp
                                @foreach($tipe_kerusakans as $tipe_kerusakan)
                                <option value="{{$tipe_kerusakan->kategori_kerusakan}}">
                                    {{$tipe_kerusakan->kategori_kerusakan}}</option>
                                @endforeach
                            </select>
                            <script>
                                var komponen = $("#komponen").val();
                                            var subKomponen = $("#sub_komponen").val();
                                            $("#sub_komponen").change(function () {
                                                $('#tipe_kerusakan').removeAttr('disabled');
                                            });
                        
                            </script>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 ">
                            <label>Progress Job</label>
                            <select name="progress_job" class="form-control" id="progress_job" disabled>
                                <option value="" selected disabled>Pilih</option>
                                @php
                                $activities = $routings->unique('activity'); $activities->values()->all();
                                @endphp
                                @foreach ($activities as $activity)
                                <option value="{{$activity->activity}}" class="activity_job"
                                    sub-komponen="{{$activity->sub_komponen}}"
                                    kategori-kerusakan="{{$activity->kategori_kerusakan}}">{{$activity->activity}}
                                </option>
                                @endforeach
                            </select>
                            <script>
                                $("#sub_komponen").change(function () {
                                                $('#progress_job').attr('disabled', 'disabled');
                                            });
                                            $("#tipe_kerusakan").change(function () {
                                                var sub_komponen = $('#sub_komponen').val();
                                                var kerusakan = $('#tipe_kerusakan').val();
                                                $('#progress_job').removeAttr('disabled');
                                                $('.activity_job').hide();
                                                $('.activity_job[sub-komponen*=' + sub_komponen + '][kategori-kerusakan*=' + kerusakan +
                                                    ']').show();
                                            });
                        
                            </script>
                            <hr>
                            <button type="submit" href="#" class="btn btn-success mx-auto form-control"
                                style="margin:10px">Tambah Data Routing PDC</button>
                        </div>
                    </div>
                </form>
            </div>

            <table class="table  col-md-6" id="dynamic_field">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Urutan Job</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody class="container1">

                    @foreach($routings as $routing)
                    @foreach($relation_routings as $relation_routing)
                    @if($routing->id == $relation_routing->routing_id)
                    <tr>
                        <td>{{$loop->index + 1}}</td>
                        <td><span>{{$routing->activity}}</span></td>
                        <form id="form-delete-{{$relation_routing->id}}"
                            action="{{route('relation_routing.destroy.pdc',$relation_routing->id)}}"
                            style="display:none" method="POST">
                            @csrf
                            @method('DELETE')
                        </form>
                        <td><a href="#" onclick="
                                            if(confirm('Yakin ingin menghapus?')){
                                                event.preventDefault();document.getElementById('form-delete-{{$relation_routing->id}}').submit();
                                            }else{
                                                event.preventDefault();
                                            }
                                                " class="fa fa-trash fa-lg" style="color:red;"></i> </a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                    @endforeach

                    {{-- <form action="{{route('relation_routing.store.pdc')}}" method="POST">
                    @csrf
                    <tr>
                        <td>#</td>
                        <td>
                            <input type="text" name="routing_pdc" list="data-routing">
                        </td>
                        <td><button type="submit">tambah</button></td>
                    </tr>
                    </form> --}}
                </tbody>
            </table>
        </div><br><br>
    </div>
    <br>
</div>
<datalist id="data-routing">
    @foreach($routings as $routing)
    <option value="{{$routing->id}}">{{$routing->activity}}</option>
    @endforeach
</datalist>
<script>
    $(document).ready(function () {
        var max_fields = 10;
        var wrapper = $(".container1");
        var add_button = $(".add_form_field");
        var x = 1;
        $(add_button).click(function (e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append(
                    '<tr id="row' + x + '"><td> ' + x +
                    ' </td><td><input class="form-control" name="routings[]" id="answer" list="data-routing"><input name="urutan[]"  type="hidden" value="' +
                    x + '"></td><td><a href="#" class="delete btn btn-danger">Delete</a></td></tr>');
            } else {
                alert('You Reached the limits')
            }
        });

        $(wrapper).on("click", ".delete", function (e) {
            e.preventDefault();
            $('#row' + x + '').remove();
            x--;
        })
    });

</script>
@endsection