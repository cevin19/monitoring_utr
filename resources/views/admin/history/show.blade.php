@extends('admin.layouts.app')

@section('content')
<button type="button" class="btn btn-success btn-circle btn-xl" style="position:fixed; bottom:10px; right:10px"><i class="fa fa-download"></i>
</button>
<div class="card">
    <div class="card-header">
        <strong class="card-title">History/Log</strong>
    </div>

    <div class="card-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pdc-tab" data-toggle="tab" href="#pdc" role="tab" aria-controls="pdc" aria-selected="true" style="font-size:20px">PDC</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="mkt-tab" data-toggle="tab" href="#mkt" role="tab" aria-controls="mkt" aria-selected="false" style="font-size:20px">MKT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="ppc-tab" data-toggle="tab" href="#ppc" role="tab" aria-controls="ppc" aria-selected="false" style="font-size:20px">PPC</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="pdc" role="tabpanel" aria-labelledby="pdc-tab">
                <br>
                @php
                $jumlah_data_r = count($relation_routings) ;

                @endphp
                @if($jumlah_data_r >= 2)
                <div class="table-stats">
                    <table style="text-align:center" id="history" class="display">
                        <thead>
                            <tr>
                                <th class="serial">#</th>
                                <th>register</th>
                                <th>unit model</th>
                                <th>nama komponen</th>
                                <th>date in</th>
                                <th>start progress</th>
                                <th>date out</th>
                                <th>lead time</th>
                                <th>status leadtime</th>
                                <th>plan ready go ahead</th>
                                <th>%tage progress</th>
                                <th>Status</th>

                            </tr>
                        </thead>

                        <tbody>

                            @foreach($pdcs as $pdc)

                            <tr>
                                <td class="serial">{{$loop->index +1}}</td>
                                <td>{{$pdc->register}}</td>
                                <td>{{$pdc->unit_model}}</td>
                                <td>{{$pdc->nama_komponen}}</td>
                                <td>{{$pdc->date_in}}</td>
                                <td>{{$pdc->start_progress}}</td>
                                <td>{{$pdc->date_out}}</td>

                                {{-- leadtime --}}
                                @php
                                $start_date = new DateTime($pdc->date_in);
                                $end_date = new DateTime($pdc->date_out);
                                $leadtime = $start_date->diff($end_date);

                                @endphp
                                <td>{{$leadtime->days}}</td>
                                {{-- end leadtime --}}

                                {{-- status leadtime --}}
                                @foreach($std_leadtimes as $std_leadtime)
                                @if($pdc->nama_komponen == $std_leadtime->nama_componen)
                                @if($leadtime->days > $std_leadtime->time)
                                <td class="bg-danger">Over</td>
                                @else
                                <td class="bg-success">Under</td>
                                @endif
                                @endif
                                @endforeach
                                {{-- plan ready go ahead --}}
                                @php
                                $totalurutan = count($relation_routings);
                                $data_routing = App\Model\routing::where('activity',$pdc->progress_job)->get()->first();
                                $data_routing = $data_routing->id;
                                $data_urutan = App\Model\Relation\relation_routings::where('routing_id',$data_routing)->get()->first();
                                $data_urutan = $data_urutan->urutan;


                                $tambahan_waktu = 0;

                                foreach($relation_routings as $relation_routing){
                                if($relation_routing->urutan >= $data_urutan){
                                $rout = App\Model\routing::where('id',$relation_routing->routing_id)->get()->first();
                                $tambahan_waktu = $tambahan_waktu + $rout->estimasi ;
                                }
                                }

                                $waktu_asal = $pdc->start_progress;
                                $carbon_date = \Carbon\Carbon::parse($waktu_asal);
                                $carbon_date->addHours($tambahan_waktu);
                                @endphp
                                <td>{{$carbon_date}}</td>
                                @php
                                $persentasi=substr(($data_urutan/$totalurutan)*100,0,4);
                                @endphp
                                @if ($persentasi <= 45) <td>
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}" aria-valuemin="0" aria-valuemax="100">{{$persentasi}}%</div>
                                    </div>
                                    </td>
                                    <td class="text-success">Open</td>
                                    @elseif ($persentasi <= 99) <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-warning" role="progressbar" style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}" aria-valuemin="0" aria-valuemax="100">{{$persentasi}}%</div>
                                        </div>
                                        </td>
                                        <td class="text-success">Open</td>
                                        @elseif ($persentasi == 100)
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}" aria-valuemin="0" aria-valuemax="100">Done!</div>
                                            </div>
                                        </td>
                                        <td class="text-danger">Close</td>
                                        @endif
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div> <!-- /.table-stats -->
                @else
                <span>Lengkapi Data Routing terlebih dahulu</span>
                @endif
            </div>
            <div class="tab-pane fade" id="mkt" role="tabpanel" aria-labelledby="mkt-tab">MKT</div>
            <div class="tab-pane fade" id="ppc" role="tabpanel" aria-labelledby="ppc-tab">PPC</div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var elemen = document.getElementsByClassName('id-routing');
        var elemen_tampil = document.getElementsByClassName('routing-tampil');



        var arr = jQuery.makeArray(elemen);
        var arr_tampil = jQuery.makeArray(elemen_tampil);
        var arrL = arr.length;
        var arr_tampilL = arr_tampil.length;

        for (i = 0; i < arrL; i++) {
            routing = $(arr[i]).text();
            for (x = 0; x < arr_tampilL; x++) {
                hasilRouting = $(arr_tampil[i]).text();
                if (routing = hasilRouting) {
                    a = x + 1;
                    datatampil = document.getElementById('estimasi-id-' + a);
                    // console.log($(datatampil).text());
                }
            }

        }
    });



    $('[data-countdown]').each(function() {
        var $this = $(this),
            finalDate = $(this).data('countdown');
        var array_waktu = finalDate.split('/');
        var waktu_awal = new Date(array_waktu[0]),
            waktu_akhir = new Date(array_waktu[1]);

        var timestamp = waktu_akhir - Date.now();
        timestamp /= 1000; // from ms to seconds

        function component(x, v) {
            return Math.floor(x / v);
        }

        var $span = $('span');

        setInterval(function() { // execute code each second

            timestamp--; // decrement timestamp with one second each second

            var days = component(timestamp, 24 * 60 * 60), // calculate days from timestamp
                hours = component(timestamp, 60 * 60) % 24, // hours
                minutes = component(timestamp, 60) % 60, // minutes
                seconds = component(timestamp, 1) % 60; // seconds


            $this.html(days + " days, " + hours + ":" + minutes + ":" + seconds);

        }, 1000); // interval each second = 1000 ms
        // var $this = $(this), finalDate = $(this).data('countdown');
        // $this.countdown(finalDate, function(event) {
        //     $this.html(event.strftime('%D days %H:%M:%S'));
        // });
    });
</script>

@endsection