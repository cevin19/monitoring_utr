@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Index Part Kit</strong>
        @if(Auth::user()->role == 'admin')
        <a class="btn btn-success float-right " style="margin-left:2px" href="{{route('partkit.create')}}">Tambah
            PartKit</a>
        <a class="btn btn-danger float-right " style="margin-left:2px" href="{{route('partkit.create_po')}}">Tambah Data
            PO</a>
        @endif
    </div>

    <div class="card-body">
        {{-- Table Pertama --}}
        <div class="table-stats ov-h">
            <table class="" style="text-align:center;" id="partkit" class="display">
                <thead>
                    <tr style="background-color:#e8e9ef">
                        <th class="serial">#</th>
                        <th>register</th>
                        <th>Pro Number</th>
                        <th>unit model</th>
                        <th>nama komponen</th>
                        <th nowrap="nowrap">req iserve</th>
                        <th>PO Number</th>
                        <th>Status PO</th>
                        <th>Std Release</th>
                        <th nowrap="nowrap">Act Release PO</th>
                        <th>lead time</th>
                        <th>status leadtime</th>
                        <th>Drawing Engineer</th>
                        <TH>Status Drawing</TH>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($partkit_satus as $partkit_satu)

                    <tr>
                        <td class="serial">{{$loop->index +1}}</td>
                        <td>{{$partkit_satu->register}}</td>
                        <td>{{$partkit_satu->pro_number}}</td>
                        <td>{{$partkit_satu->unit_model}}</td>
                        <td>{{$partkit_satu->nama_komponen}}</td>
                        @php
                        $req_iserve = \Carbon\Carbon::parse($partkit_satu->req_iserve)->format('d-M-y');
                        @endphp
                        <td nowrap="nowrap">{{$req_iserve}}</td>

                        @if($partkit_satu->po_number != null)
                        <td nowrap="nowrap">{{$partkit_satu->po_number}}</td>
                        @else
                        {{-- <form id="form-update-po-number-{{$partkit_satu->id}}"
                        action="{{route('partkit_po.po_number.update',$partkit_satu->id)}}" style="display:none"
                        method="POST">
                        @csrf
                        <input name="act_release_po" type="hidden" value="{{Carbon\Carbon::now()->format('Y-m-d')}}">
                        @method('PATCH')
                        </form> --}}
                        <td>
                            @if ($partkit_satu->act_release_po == null)
                            @if(Auth::user()->role == 'admin')
                            <a href="#" data-target="#partkit_modal{{$partkit_satu->id}}" data-toggle="modal"
                                class="text-danger">PO&nbsp;Number</a>
                            @else
                            -
                            @endif
                            @else
                            -
                            @endif
                        </td>
                        <div id="partkit_modal{{$partkit_satu->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">PO Number</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <form action="{{route('partkit_po.update.po_number', $partkit_satu->id)}}"
                                        method="POST">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Register</label>
                                                    <input type="text" class="form-control"
                                                        value="{{$partkit_satu->register}}" disabled>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Pro Number</label>
                                                    <input type="text" class="form-control"
                                                        value="{{$partkit_satu->pro_number}}" disabled>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Unit Model</label>
                                                    <input type="text" class="form-control"
                                                        value="{{$partkit_satu->unit_model}}" disabled>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Nama Komponen</label>
                                                    <input type="text" class="form-control"
                                                        value="{{$partkit_satu->nama_komponen}}" disabled>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>PO Number</label>
                                                    <input type="text" class="form-control" placeholder="ex: 92000xx"
                                                        name="po_number">
                                                    @method('PATCH')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger">Kirim!</button>
                                            <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        @endif

                        @if($partkit_satu->po_number == null)
                        <td class="alert alert-danger" role="alert" style="color:#721c24">Pending</td>
                        @else
                        <td class="alert alert-success" role="alert" style="color:#155724">Done</td>
                        @endif
                        <td>{{$partkit_satu->std_release_po}}</td>

                        @if($partkit_satu->act_release_po != null)
                        @php
                        $act_release_po = \Carbon\Carbon::parse($partkit_satu->act_release_po)->format('d-M-y');
                        @endphp
                        <td nowrap="nowrap">{{$act_release_po}}</td>
                        @else
                        <form id="form-update-act-release-po-{{$partkit_satu->id}}"
                            action="{{route('partkit_po.update.act_release_po',$partkit_satu->id)}}"
                            style="display:none" method="POST">
                            @csrf
                            <input name="act_release_po" type="hidden"
                                value="{{Carbon\Carbon::now()->format('Y-m-d')}}">
                            @method('PATCH')
                        </form>
                        <td>
                            @if ($partkit_satu->act_release_po == null)
                            @if(Auth::user()->role == 'admin')
                            <a href="#" class="text-danger" onclick="
                                        if(confirm('Kirim Info Act Release PO?')){
                                            event.preventDefault();document.getElementById('form-update-act-release-po-{{$partkit_satu->id}}').submit();
                                        }else{
                                            event.preventDefault();
                                        }">Act Release PO</a>
                            @else
                            -
                            @endif
                            @else
                            -
                            @endif
                        </td>
                        @endif
                        {{-- leadtime --}}
                        @php
                        $start_date = new DateTime($partkit_satu->req_iserve);
                        $end_date = new DateTime($partkit_satu->date_out);
                        $leadtime = $start_date->diff($end_date);
                        @endphp

                        <td>{{$leadtime->days}}</td>
                        {{-- end leadtime --}}

                        {{-- status leadtime --}}
                        @if($leadtime->days > $partkit_satu->std_release_po)
                        <td class="alert alert-danger" role="alert" style="color:#721c24">Over</td>
                        @elseif($partkit_satu->act_release_po == null)
                        <td class="alert" role="alert" style="color:#383d41">-</td>
                        @else
                        <td class="alert alert-success" role="alert" style="color:#155724">Under</td>
                        @endif

                        {{-- Drawing Engine --}}
                        <td>
                            @if($partkit_satu->drawing_engineer == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <i class="text-danger fa fa-times"></i>
                            @endif
                        </td>

                        {{-- STatus Drawing --}}
                        <td>
                            @if($partkit_satu->status_drawing == "OK")
                            <i class="text-success fa fa-check"></i>
                            @else
                            <i class="text-danger fa fa-times"></i>
                            @endif
                        </td>


                        <td>
                            <a href="{{route('PDC.edit',$partkit_satu->id)}}" class="fa fa-edit fa-lg"
                                style="color:orange;"></i> </a>
                            <form id="form-delete-{{$partkit_satu->id}}"
                                action="{{route('PDC.destroy',$partkit_satu->id)}}" style="display:none" method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                            <a href="#"
                                onclick="if(confirm('Yakin ingin menghapus?')){event.preventDefault();document.getElementById('form-delete-{{$partkit_satu->id}}').submit();}else{event.preventDefault();}"
                                class="fa fa-trash fa-lg" style="color:red;">
                                </i>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <br><br>
        {{-- Table kedua --}}
        <div class="table-stats ov-h">
            <table class="" style="text-align:center;" id="partkit2" class="display">
                <thead>
                    <tr style="background-color:#e8e9ef">
                        <th class="serial">#</th>
                        <th>Register</th>
                        <th>Pro Number</th>
                        <th>Unit Model</th>
                        <th>Nama Komponen</th>
                        <th>Nama Mesin</th>
                        <th>Man Power</th>
                        <th>Progress Job</th>
                        <th>Std Routing</th>
                        <th>Start Progress (jam mulai)</th>
                        <th>Aktual Finish (jam finish)</th>
                        <th>Real Time</th>
                        <th>plan ready</th>
                        <th>Status Pekerjaan</th>
                        @if(Auth::user()->role == 'admin')
                        <th>Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($partkits as $partkit)
                    <tr>
                        <td class="serial">{{$loop->index +1}}</td>
                        <td>{{$partkit->register}}</td>
                        <td>{{$partkit->pro_number}}</td>
                        <td>{{$partkit->unit_model}}</td>
                        <td>{{$partkit->nama_komponen}}</td>
                        <td>{{$partkit->nama_mesin}}</td>
                        <td>{{$partkit->man_power}}</td>
                        @foreach($routings as $routing)
                        @if($routing->id == $partkit->routing_id)
                        <td>{{$routing->activity}}</td>
                        <td>{{$routing->estimasi}}</td>
                        @if($partkit->start_progress != null)
                        <td nowrap="nowrap">
                            @php
                            $start_progress = \Carbon\Carbon::parse($partkit->start_progress)->format('d-M-y
                            H:i:s');
                            @endphp
                            {{
                            substr($start_progress,0,10)
                            }}
                            <br>
                            {{
                            substr($start_progress,10)
                            }}
                        </td>
                        @else
                        {{-- <form id="form-update-start-progress-{{$partkit->id}}"
                        action="{{route('partkit.start-progress.update',$partkit->id)}}" style="display:none"
                        method="POST">
                        @csrf
                        <input name="start_progress" type="hidden" value="{{Carbon\Carbon::now()}}">
                        @method('PATCH')
                        </form> --}}
                        <td>
                            <a href="#" onclick="
                            if(confirm('Kirim Info Start Progress?')){
                                event.preventDefault();document.getElementById('form-update-start-progress-{{$partkit->id}}').submit();
                            }else{
                                event.preventDefault();
                            }
                                ">Start Progress</a>
                        </td>
                        @endif

                        {{-- end progress --}}
                        @if($partkit->end_progress != null)
                        <td nowrap="nowrap">
                            @php
                            $end = \Carbon\Carbon::parse($partkit->end_progress)->format('d-M-y H:i:s');
                            @endphp
                            {{
                                substr($end,0,10)
                                }}
                            <br>
                            {{
                                substr($end,10)
                                }}
                        </td>
                        @else
                        {{-- <form id="form-update-end-progress-{{$partkit->id}}"
                        action="{{route('partkit.end-progress.update',$partkit->id)}}" style="display:none"
                        method="POST">
                        @csrf
                        <input id="input-end-progress-{{$partkit->id}}" name="end_progress" type="hidden" value="">
                        @method('PATCH')
                        </form> --}}
                        <td>
                            <a href="#" onclick="
                                if(confirm('Kirim Info Start Progress?')){
                                    event.preventDefault();document.getElementById('form-update-end-progress-{{$partkit->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }
                                    ">End Progress</a>
                        </td>
                        @endif

                        {{-- realtime --}}
                        <td>
                            {{--                            
                            @if($partkit->done == 0 && $partkit->start_progress != null)
                            <div id="dataCountdown{{$partkit->id}}"
                            data-countdown="{{$waktu_total}}|{{$partkit->id}}|{{$total_pause}}">
        </div>
        @else
        -
        @endif
        @else --}}-
        </td>

        <td>
            @if($partkit->routing_id != null && $partkit->start_progress != null)
            {{($partkit->start_progress)->addHours($routing->estimasi)}}
            @else
            -
            @endif
        </td>
        <td>
            under
        </td>
        @if(Auth::user()->role == 'admin')
        <td>
            <a href="{{route('PDC.edit',$partkit->id)}}" class="fa fa-edit fa-lg" style="color:orange;"></i> </a>
            <form id="form-delete-{{$partkit->id}}" action="{{route('PDC.destroy',$partkit->id)}}" style="display:none"
                method="POST">
                @csrf
                @method('DELETE')
            </form>
            <a href="#"
                onclick="if(confirm('Yakin ingin menghapus?')){event.preventDefault();document.getElementById('form-delete-{{$partkit->id}}').submit();}else{event.preventDefault();}"
                class="fa fa-trash fa-lg" style="color:red;">
                </i>
            </a>
        </td>
        @endif
        </tr>

        @endif
        @endforeach
        @endforeach
        </tbody>
        </table>
    </div>

</div>


@endsection