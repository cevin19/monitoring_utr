@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Edit PDC {{$pdc->register}}</strong>
        <a class="btn btn-warning float-right" href="{{route('PDC.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        <form action="{{route('PDC.update',$pdc->id)}}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Register:</label>
                            <input type="text" name="register" class="form-control" placeholder="ex: BPC190001" value="{{$pdc->register}}">
                        </div>
                        <div class="col-md-6">
                            <label>Unit Model</label>
                            <input type="text" name="unit_model" class="form-control" placeholder="ex: PC2000" value="{{$pdc->unit_model}}">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nama Komponen</label>
                            <input type="text" name="nama_komponen" class="form-control" placeholder="ex: BUC" value="{{$pdc->nama_komponen}}">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Progress Job</label>
                            <select name="progress_job" class="form-control">
                                <option value="" selected disabled>Pilih</option>
                            @foreach($routings as $routing)
                                <option value="{{$routing->activity}}" @if($pdc->progress_job == $routing->activity) selected @endif>{{$routing->activity}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Date In</label>
                            <input type="date" name="date_in" class="form-control" placeholder="Date In" value="{{$pdc->date_in}}">
                        </div>
                        <div class="col-md-6">
                            <label>Date Out</label>
                            <input type="date" name="date_out" class="form-control" placeholder="Date Out" value="{{$pdc->date_out}}">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Start Progress</label>
                            {{-- yang ini belum bisa ngasi value start progress daff --}}
                            <input type="datetime-local" name="start_progress" class="form-control" placeholder="Start Progress" >
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-warning form-control col-md-10">Update</button>
        </form>

    </div>
</div>
<datalist id="data-routing">
    @foreach($routings as $routing)
    <option value="{{$routing->id}}">{{$routing->activity}}</option>
    @endforeach
</datalist>
@endsection