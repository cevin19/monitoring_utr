@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Add New PartKit PO</strong>
        <a class="btn btn-warning float-right" href="{{route('partkit.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        <form action="{{route('partkit.store_po')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Register:</label>
                            <input type="text" name="register" class="form-control" placeholder="ex: BPC190001">
                        </div>
                        <div class="col-md-6">
                            <label>Unit Model</label>
                            <input type="text" name="unit_model" class="form-control" placeholder="ex: PC2000">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-5">
                            <label>Nama Komponen</label>
                            <input type="text" name="nama_komponen" class="form-control" placeholder="ex: BUC">
                        </div>
                        <div class="col-md-7">
                            <label>Std Release PO (Hari)</label>
                            <div class="input-group mb-3">
                                <input type="number" name="std_release_po" class="form-control" placeholder="ex: 1"
                                    aria-label="std-release-po" aria-describedby="std-release">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="std-release">Hari</span>
                                </div>
                            </div>
                            <label class="text-danger">*Std Release default = 1</label>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Req Iserve</label>
                            <input type="date" name="req_iserve" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label>PO Number</label>
                            <input type="text" class="form-control" name="po_number" placeholder="ex: 92000xxx">
                        </div>
                        <div class="col-md-12" style="text-align:center;">
                            <label class="text-danger">*Jika belum ada, kosongkan data diatas</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Pro Number</label>
                            <input type="text" name="pro_number" class="form-control" placeholder="ex: 66729xx">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="margin-left:170px;">Status Drawing</label>
                            <br>
                            <div class="table-stats order-table ov-h" style="margin-left:50px;">
                                <table style="text-align:center; width:100%;">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">List</th>
                                            <th colspan="2" style="padding-right:70px">Status</th>
                                        </tr>
                                        <tr>
                                            <th style="padding-left:50px">Ok</th>
                                            <th style="padding-right:40px">Not</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Drawing Engineer</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="drawing_engineer" value="OK">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="drawing_engineer" value="Not" checked>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status Drawing</td>
                                            <td style="padding-left:50px">
                                                <input type="radio" name="status_drawing" value="Finish">
                                            </td>
                                            <td style="padding-right:50px">
                                                <input type="radio" name="status_drawing" value="Not" checked>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <label class="text-danger">*Select pada lingkaran untuk menambahkan status</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-success form-control col-md-10">Tambah</button>
        </form>

    </div>
</div>
@endsection