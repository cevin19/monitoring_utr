@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Index MKT</strong>
        @if(Auth::user()->role == 'admin')
        <a class="btn btn-danger float-right " style="margin-left:2px" href="{{route('MKT.create')}}">Tambah MKT</a>
        @endif
        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#routing">
            List Progress Job
        </button>
    </div>

    <div class="card-body">
        @php
        $jumlah_data_r = count($relation_routings) ;
        @endphp
        @if($jumlah_data_r >= 2)
        <div class="table-stats ov-h">
            <table class="" style="text-align:center" id="mkt" class="display">
                <thead>
                    <tr style="background-color:#e8e9ef">
                        <th class="serial">#</th>
                        <th>register</th>
                        <th>unit model</th>
                        <th>nama komponen</th>
                        <th nowrap="nowrap">date in</th>
                        <th>progress job</th>
                        <th>routing</th>
                        <th>start progress</th>
                        <th nowrap="nowrap">date out</th>
                        <th>lead time</th>
                        <th>std leadtime</th>
                        <th>status leadtime</th>
                        <th>plan ready go ahead</th>
                        <TH>Real Time</TH>
                        <th>%tage progress</th>
                        @if(Auth::user()->role == 'admin')
                        <th>Action</th>
                        @endif
                    </tr>
                </thead>

                <tbody>

                    @foreach($mkts as $mkt)

                    <tr>
                        <td class="serial">{{$loop->index +1}}</td>
                        <td>{{$mkt->register}}</td>
                        <td>{{$mkt->unit_model}}</td>
                        <td>{{$mkt->nama_komponen}}</td>
                        @php
                        $date_in = \Carbon\Carbon::parse($mkt->date_in)->format('d-M-y');
                        @endphp
                        <td nowrap="nowrap">{{$date_in}}</td>

                        {{-- progress job --}}
                        @php
                        $totalurutan = count($relation_routings);
                        $data_routing = $mkt->routing_id;
                        $data_urutan =
                        App\Model\Relation\relation_routings::where(['routing_id'=>$data_routing, 'table_name'=> 'mkt'
                        ])->get()->first();
                        $data_urutan = $data_urutan->urutan;
                        $total_estimasi = 0;
                        foreach($relation_routings as $relation_routing){
                        if($relation_routing->urutan <= $data_urutan){
                            $estimasi_routing=App\Model\routing::where('id',$relation_routing->
                            routing_id)->get()->first();
                            $total_estimasi= $total_estimasi + $estimasi_routing->estimasi;
                            }
                            }
                            if($data_urutan < $totalurutan){
                                $relation_routing_a=App\Model\Relation\relation_routings::where(['table_name'=>
                                'mkt','urutan'=>$data_urutan+1])->
                                get()->first();
                                $relation_routing_b =$relation_routing_a->routing_id;
                                $data_selanjutnya = App\Model\Routing::where('id',$relation_routing_b)->get()->first();

                                }else{
                                $relation_routing_a =
                                App\Model\Relation\relation_routings::where(['table_name'=>'mkt','urutan'=>$data_urutan])->get()->first();
                                $relation_routing_b =$relation_routing_a->routing_id;
                                $data_selanjutnya = App\Model\Routing::where('id',$relation_routing_b)->get()->first();
                                }
                                
                                @endphp
                                <form id="mkt-routing-update-{{$mkt->id}}"
                                    action="{{route('MKT.progress-job.update',$mkt->id)}}" method="POST"
                                    style="display:none">
                                    @csrf @method('PATCH')
                                    <input type="hidden" name="activity" id="" value="{{$data_selanjutnya->activity}}">
                                    <input type="hidden" name="routing_id" id="" value="{{$data_selanjutnya->id}}">
                                </form>
                                <td>
                                    <div routing-countdownn="{{$mkt->start_progress.'/'.$total_estimasi.'/'.$mkt->id}}">
                                    </div>
                                    {{$mkt->progress_job}}
                                </td>

                                {{-- sisa routing --}}
                                @php
                                $sisa = 0;
                                foreach($relation_routings as $relation_routing){
                                if($relation_routing->urutan > $data_urutan){
                                $sisa++;
                                }
                                }
                                @endphp
                                <td id="sisa">{{$sisa}}</td>

                                @if($mkt->start_progress != null)
                                <td nowrap="nowrap">
                                    @php
                                    $start_progress = \Carbon\Carbon::parse($mkt->start_progress)->format('d-M-y
                                    H:i:s');
                                    @endphp
                                    {{
                            substr($start_progress,0,10)
                            }}
                                    <br>
                                    {{
                                substr($start_progress,10)
                                }}
                                </td>
                                @else
                                <form id="form-update-start-progress-{{$mkt->id}}"
                                    action="{{route('MKT.start-progress.update',$mkt->id)}}" style="display:none"
                                    method="POST">
                                    @csrf
                                    <input name="start_progress" type="hidden" value="{{Carbon\Carbon::now()}}">
                                    @method('PATCH')
                                </form>
                                <td>
                                    <a href="#" onclick="
                                if(confirm('Kirim Info Start Progress?')){
                                    event.preventDefault();document.getElementById('form-update-start-progress-{{$mkt->id}}').submit();
                                }else{
                                    event.preventDefault();
                                }
                                    ">Start Progress</a>
                                </td>
                                @endif

                                @if($mkt->date_out != null)
                                @php
                                $date_out = \Carbon\Carbon::parse($mkt->date_out)->format('d-M-y');
                                @endphp
                                <td nowrap="nowrap">{{$date_out}}</td>
                                @else
                                <form id="form-update-date-out-{{$mkt->id}}"
                                    action="{{route('MKT.date-out.update',$mkt->id)}}" style="display:none"
                                    method="POST">
                                    @csrf
                                    <input name="date_out" type="hidden"
                                        value="{{Carbon\Carbon::now()->format('Y-m-d')}}">
                                    @method('PATCH')
                                </form>
                                <td>
                                    @php
                                    $totalurutan = count($relation_routings);
                                    $data_routing =
                                    App\Model\routing::where('activity',$mkt->progress_job)->get()->first();
                                    $data_routing = $data_routing->id;
                                    $data_urutan =
                                    App\Model\Relation\relation_routings::where(['routing_id'=>$data_routing,
                                    'table_name'=> 'mkt' ])->get()->first();
                                    $data_urutan = $data_urutan->urutan;


                                    $tambahan_waktu = 0;

                                    foreach($relation_routings as $relation_routing){
                                    if($relation_routing->urutan >= $data_urutan){
                                    $rout =
                                    App\Model\routing::where('id',$relation_routing->routing_id)->get()->first();
                                    $tambahan_waktu = $tambahan_waktu + $rout->estimasi ;
                                    }
                                    }
                                    $waktu_asal = $mkt->start_progress;
                                    $carbon_date = \Carbon\Carbon::parse($waktu_asal);
                                    $carbon_date = $carbon_date->addHours($tambahan_waktu)->format('d-M-y H:i:s');

                                    $persentasi=substr(($data_urutan/$totalurutan)*100,0,4);
                                    @endphp
                                    @if ($mkt->done == 0)
                                    @if(Auth::user()->role == 'admin')
                                    <a href="#" class="text-danger" onclick="
                                    if(confirm('Kirim Info Date Out?')){
                                        event.preventDefault();document.getElementById('form-update-date-out-{{$mkt->id}}').submit();
                                    }else{
                                        event.preventDefault();
                                    }">Date Out</a>
                                    @else
                                    -
                                    @endif
                                    @else
                                    -
                                    @endif
                                </td>
                                @endif
                                {{-- leadtime --}}
                                @php
                                $start_date = new DateTime($mkt->date_in);
                                $end_date = new DateTime($mkt->date_out);
                                $leadtime = $start_date->diff($end_date);
                                @endphp

                                <td>{{$leadtime->days}}</td>
                                {{-- end leadtime --}}

                                {{-- standart leadtime --}}
                                @foreach($std_leadtimes as $std_leadtime)
                                @if($mkt->nama_komponen == $std_leadtime->nama_componen)
                                <td>{{$std_leadtime->time}}</td>
                                {{-- @else
                                                <td>Belum Ada Stardart</td> --}}
                                @endif
                                @endforeach
                                {{-- status leadtime --}}
                                @foreach($std_leadtimes as $std_leadtime)
                                @if($mkt->nama_komponen == $std_leadtime->nama_componen)
                                @if($leadtime->days > $std_leadtime->time)
                                <td class="alert alert-danger" role="alert" style="color:#721c24">Over</td>
                                @elseif($mkt->date_out == null)
                                <td class="alert" role="alert" style="color:#383d41">-</td>
                                @else
                                <td class="alert alert-success" role="alert" style="color:#155724">Under</td>
                                @endif
                                @endif
                                @endforeach
                                {{-- plan ready go ahead --}}
                                @php
                                $totalurutan = count($relation_routings);
                                $data_routing = $mkt->routing_id;
                                $data_urutan =
                                App\Model\Relation\relation_routings::where(['routing_id'=>$data_routing, 'table_name'=>
                                'mkt' ])->get()->first();
                                $data_urutan = $data_urutan->urutan;


                                $tambahan_waktu = 0;

                                foreach($relation_routings as $relation_routing){
                                if($relation_routing->urutan >= $data_urutan){
                                $rout = App\Model\routing::where('id',$relation_routing->routing_id)->get()->first();
                                $tambahan_waktu = $tambahan_waktu + $rout->estimasi ;
                                }
                                }
                                $waktu_asal = $mkt->start_progress;
                                $carbon_date = \Carbon\Carbon::parse($waktu_asal);
                                $carbon_date = $carbon_date->addHours($tambahan_waktu);

                                $persentasi=substr(($data_urutan/$totalurutan)*100,0,4);
                                @endphp
                                @if($mkt->start_progress != null)
                                <td nowrap="nowrap">
                                    {{
                                substr($carbon_date,0,10)
                                }}
                                    <br>
                                    {{
                                    substr($carbon_date,10)
                                    }}
                                </td>
                                @else
                                <td>-</td>
                                @endif

                                {{-- realtime --}}
                                <td>
                                    @if($mkt->done == 0 && $mkt->start_progress!=null)
                                    <div id="dataCountdown{{$mkt->id}}" data-countdown="{{$carbon_date}}|{{$mkt->id}}">
                                    </div>
                                    @else
                                    -
                                    @endif
                                </td>

                                @if($mkt->done == 0)
                                @if ($persentasi <= 45) <td>
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar"
                                            style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}"
                                            aria-valuemin="0" aria-valuemax="100">{{$persentasi}}%</div>
                                    </div>
                                    </td>
                                    @elseif ($persentasi <= 79) <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-warning" role="progressbar"
                                                style="width: {{$persentasi}}%;color:white;"
                                                aria-valuenow="{{$persentasi}}" aria-valuemin="0" aria-valuemax="100">
                                                {{$persentasi}}%</div>
                                        </div>
                                        </td>
                                        @elseif ($persentasi <= 99) <td>
                                            <div class="progress">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: {{$persentasi}}%;color:white;"
                                                    aria-valuenow="{{$persentasi}}" aria-valuemin="0"
                                                    aria-valuemax="100">
                                                    {{$persentasi}}%</div>
                                            </div>
                                            </td>
                                            @elseif ($persentasi == 100)
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#ready-{{$mkt->id}}"
                                                    onclick="ready({{$mkt->id}})">
                                                    <div class="progress">
                                                        <div id="progbar-{{$mkt->id}}" class="progress-bar bg-info"
                                                            role="progressbar" style="width: 99%;color:white;"
                                                            aria-valuenow="{{$persentasi}}" aria-valuemin="0"
                                                            aria-valuemax="100">Last Job</div>
                                                    </div>
                                                </a>
                                            </td>
                                            <!-- Modal Ready -->
                                            <div class="modal fade" id="ready-{{$mkt->id}}" tabindex="-1" role="dialog"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Info
                                                                Keterlaksanaan</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body" id="terlambat-{{$mkt->id}}">
                                                            <form id="form-done-{{$mkt->id}}"
                                                                action="{{route('MKT.done.update',$mkt->id)}}"
                                                                method="POST" style="display:none">
                                                                @csrf @method('PATCH')
                                                                <label>Masukan info keterlambatan</label>
                                                                <input type="text" id="input-done-{{$mkt->id}}"
                                                                    name="laporan_keterlambatan"
                                                                    placeholder="Alasan Terlambat" class="form-control">
                                                                <label class="text-danger">*hanya diisi jika
                                                                    Terlambat</label>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-success"
                                                                onclick="done({{$mkt->id}})">Done!</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @endif
                                            @else
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar"
                                                        style="width: 100%;color:white;" aria-valuenow="{{$persentasi}}"
                                                        aria-valuemin="0" aria-valuemax="100">Done!</div>
                                                </div>
                                            </td>
                                            @endif
                                            @if(Auth::user()->role == 'admin')
                                            <td>
                                                <a href="{{route('MKT.edit',$mkt->id)}}" class="fa fa-edit fa-lg"
                                                    style="color:orange;"></i> </a>
                                                <form id="form-delete-{{$mkt->id}}"
                                                    action="{{route('MKT.destroy',$mkt->id)}}" style="display:none"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                <a href="#"
                                                    onclick="if(confirm('Yakin ingin menghapus?')){event.preventDefault();document.getElementById('form-delete-{{$mkt->id}}').submit();}else{event.preventDefault();}"
                                                    class="fa fa-trash fa-lg" style="color:red;">
                                                    </i>
                                                </a>

                                            </td>
                                            @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div> <!-- /.table-stats -->
        @else
        <span>Lengkapi Data Routing terlebih dahulu</span>
        @endif
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="routing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Progress Job</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table mx-auto col-md-6" id="dynamic_field">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Urutan Job</th>
                            <th scope="col">Routing</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($relation_routings as $relation_routing)
                        @foreach($routings as $routing)
                        @if($relation_routing->routing_id== $routing->id )
                        <tr>
                            <td>{{$loop->index + 1}}</td>
                            <td class="id-routing">{{$routing->activity}}</td>
                            <td>
                                <div class="estimasi-routing" id="estimasi-id-{{$loop->index + 1}}">
                                    {{$routing->estimasi}}</div>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a class="btn btn-warning float-left " style="margin-left:2px" href="{{route('routing-edit.mkt')}}">Edit
                    Routing</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-countdown]').each(function () {
        var $this = $(this),
            finalDate = $(this).data('countdown');
        var array_waktu = finalDate.split('|'),
            waktu_akhir = new Date(array_waktu[0]),
            mkt_id = array_waktu[1];

        var timestamp = new Date(waktu_akhir) - Date.now();
        timestamp /= 1000;

        function component(x, v) {
            return Math.floor(x / v);
        }
        setInterval(function () {

            timestamp--;

            var days = component(timestamp, 24 * 60 * 60),
                hours = component(timestamp, 60 * 60) % 24,
                minutes = component(timestamp, 60) % 60,
                seconds = component(timestamp, 1) % 60;

            if (timestamp > 0) {
                $this.html(days + " days, " + hours + ":" + minutes + ":" + seconds);
            } else {
                $this.html(days + " days, " + hours + ":" + minutes + ":" + seconds);
                $('#progbar-' + mkt_id).html('Ready!');
                $('#progbar-' + mkt_id).removeClass('bg-info');
                $('#progbar-' + mkt_id).addClass('bg-primary');
            }

        }, 1000);


    });



    $('[routing-countdownn]').each(function () {
        var $this = $(this),
            finalDate = $(this).attr('routing-countdownn');
        var array_waktu = finalDate.split('/'),
            start_progress = new Date(array_waktu[0]),
            total_estimasi = array_waktu[1],
            mkt_id = array_waktu[2];

        var waktu_akhir = (start_progress / 1000) + (total_estimasi * 3600);
        var timestamp = waktu_akhir - (Date.now() / 1000);




        function component(x, v) {
            return Math.floor(x / v);
        }
        setInterval(function () {

            timestamp--;

            var days = component(timestamp, 24 * 60 * 60),
                hours = component(timestamp, 60 * 60) % 24,
                minutes = component(timestamp, 60) % 60,
                seconds = component(timestamp, 1) % 60;



        }, 1000);

        setInterval(function () {
            if (timestamp < 0) {
                $('#mkt-routing-update-' + mkt_id).submit();
            }

        }, 60000);
    });
        function done(mkt_id){
            data_countdown = $('#dataCountdown'+mkt_id).html();
            status  = data_countdown.indexOf('-');
            form    = $('#form-done-'+mkt_id);
            input   = $('#input-done-'+mkt_id);
            isi_input = input.val();
            
            if(status<0){
                input.val('-');
                form.submit();
            }else{
                laporan = isi_input;
                input.val(data_countdown+'|'+laporan);
                form.submit();
            }

        }
        function ready(mkt_id){
            data_countdown = $('#dataCountdown'+mkt_id).html();
            status  = data_countdown.indexOf('-');
            
            if(status<0){
                $('#terlambat-'+mkt_id).addClass('hilang');
            }
        }
</script>

@endsection