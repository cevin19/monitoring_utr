@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Add New MKT</strong>
        <a class="btn btn-warning float-right" href="{{route('MKT.index')}}">Back</a>
    </div>

    <div class="card-body">
        @include('includes.error')
        <form action="{{route('MKT.store')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Register:</label>
                            <input type="text" name="register" class="form-control" placeholder="ex: BPC190001">
                        </div>
                        <div class="col-md-6">
                            <label>Unit Model</label>
                            <input type="text" name="unit_model" class="form-control" placeholder="ex: PC2000">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nama Komponen</label>
                            <input type="text" name="nama_komponen" class="form-control" placeholder="ex: BUC">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Progress Job</label>
                            <select name="progress_job" class="form-control">
                                <option value="" selected disabled>Pilih</option>
                                @foreach($relation_routings as $relation_routing)
                                @foreach($routings as $routing)
                                @if($relation_routing->routing_id== $routing->id )
                                <option value="{{$routing->id}}">{{$routing->activity}}</option>
                                @endif
                                @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Date In</label>
                            <input type="date" name="date_in" class="form-control" placeholder="Date In">
                        </div>
                        {{-- <div class="col-md-6">
                            <label>Date Out</label>
                            <input type="date" name="date_out" class="form-control" placeholder="Date Out">
                        </div> --}}
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Start Progress</label>
                            <input type="datetime-local" name="start_progress" class="form-control"
                                placeholder="Start Progress">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-success form-control col-md-10">Tambah</button>
        </form>

    </div>
</div>
<datalist id="data-routing">
    @foreach($routings as $routing)
    <option value="{{$routing->id}}">{{$routing->activity}}</option>
    @endforeach
</datalist>
@endsection