<header id="header" class="header">
    <div class="top-left">
        <div class="navbar-header">
            <a style="padding-left:20px; font-size:25px" class="navbar-brand" href="{{route('admin.home')}}"><b
                    class="text-danger">UTR</b> <b class="text-dark">
                    @if(Auth::user()->role == 'admin')
                    Admin
                    @else
                    Customer
                    @endif
                </b></a>
            <a class="navbar-brand hidden" href="{{route('admin.home')}}"><img src="{{asset('admin/images/logo2.png')}}"
                    alt="Logo"></a>
            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
        </div>
    </div>
    <div class="top-right">
        <div class="header-menu">
            <div class="header-left">
                <button class="search-trigger"><i class="fa fa-search"></i></button>
                <div class="form-inline">
                    <form class="search-form">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                        <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</header>