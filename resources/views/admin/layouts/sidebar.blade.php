<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="menu-title">Menu</li>
                <li class="active">
                    <a href="{{route('PDC.index')}}" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-home text-primary"></i>PDC/Warehouse</a>
                </li>
                <li class="active">
                    <a href="{{route('MKT.index')}}" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-facebook text-warning"></i>MKT/Marketing</a>
                </li>
                <li class="active">
                    <a href="{{route('PPC.index')}}" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-wrench text-success"></i>PPC/Disassy</a>
                </li>
                <li class="active">
                    <a href="{{route('machining.index')}}" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-fax text-danger"></i>Machining</a>
                </li>
                <li class="active">
                    <a href="{{route('partkit.index')}}" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-gear text-info"></i>Part Kit</a>
                </li>
                <li class="active">
                    <a href="{{route('assembly.index')}}" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-gears text-success"></i>Assembly</a>
                </li>
                <li class="menu-title">Opsi</li>
                <li class="active">
                    <a href="{{route('routing.index')}}" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-train text-warning"></i>Routing</a>
                </li>
                @if(Auth::user()->role == 'admin')
                <li class="active">
                    <a href="{{route('history.index')}}" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-history text-danger"></i>History / Log</a>
                </li>
                @endif
                <li class="active">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" style="color:#a1a1a1"><i
                            class="menu-icon fa fa-power-off"></i>Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>