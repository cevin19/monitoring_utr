<!-- Argon Scripts -->
<!-- Core -->
<script src="{{asset('home/assets/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('home/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<!-- Optional JS -->
<script src="{{asset('home/assets/vendor/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{asset('home/assets/vendor/chart.js/dist/Chart.extension.js')}}"></script>
<!-- Argon JS -->
<script src="{{asset('home/assets/js/argon.js?v=1.0.0')}}"></script>