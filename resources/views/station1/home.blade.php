@extends('station1.layouts.app')
@section('content')
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="#">Home</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-warning pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
              <a href="{{route('PDC.index')}}" style="display:contents">
                <div class="col-xl-4 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0" style="padding:20px;">
                        <div class="card-body">
                        <div class="row">
                            <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Warehouse / PDC</h5>
                            <span class="h2 font-weight-bold mb-0">0 Data</span>
                            </div>
                            <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fas fa-home"></i>
                            </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 0 Data hari ini</span>
                        </p>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" style="display:contents">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0" style="padding:20px">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Marketing / SLS</h5>
                      <span class="h2 font-weight-bold mb-0">0 Data</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-city"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 0 Data hari ini</span>
                  </p>
                </div>
              </div>
            </div>
            </a>
            <a href="#" style="display:contents">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0" style="padding:20px">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">PPC / Disassy</h5>
                      <span class="h2 font-weight-bold mb-0">0 Data</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-screwdriver"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-warning mr-2"><i class="fas fa-arrow-down"></i> 0 Data hari ini</span>
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
          <br>
          <div class="row">
          <a href="#" style="display:contents">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0" style="padding:20px">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">QA</h5>
                      <span class="h2 font-weight-bold mb-0">0 Data</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 0 Data hari ini</span>
                  </p>
                </div>
              </div>
            </div>
          </a>
          <a href="#" style="display:contents">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0" style="padding:20px">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Engineer</h5>
                      <span class="h2 font-weight-bold mb-0">0 Data</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-primary text-white rounded-circle shadow">
                          <i class="fab fa-searchengin"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 0 Data hari ini</span>
                  </p>
                </div>
              </div>
            </div>
            </a>
            <a href="#" style="display:contents">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0" style="padding:20px">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Part Kit</h5>
                      <span class="h2 font-weight-bold mb-0">0 Data</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-toolbox"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-warning mr-2"><i class="fas fa-arrow-down"></i> 0 Data hari ini</span>
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
          <br>
          <div class="row">
          <a href="#" style="display:contents">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0" style="padding:20px">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Test Bench</h5>
                      <span class="h2 font-weight-bold mb-0">0 Data</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-wrench"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 0 Data hari ini</span>
                  </p>
                </div>
              </div>
            </div>
          </a>
          <a href="#" style="display:contents">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0" style="padding:20px">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Painting & Completed</h5>
                      <span class="h2 font-weight-bold mb-0">0 Data</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-paint-roller"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 0 Data hari ini</span>
                  </p>
                </div>
              </div>
            </div>
          </a>
          <a href="#" style="display:contents">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0" style="padding:20px">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Rfu & Closing PRO61</h5>
                      <span class="h2 font-weight-bold mb-0">0 Data</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                        <i class="fas fa-universal-access"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-warning mr-2"><i class="fas fa-arrow-down"></i> 0 Data hari ini</span>
                  </p>
                </div>
              </div>
            </div>
          </a>
          </div>
          

        </div>
      </div>
    </div>
  </div>

@endsection