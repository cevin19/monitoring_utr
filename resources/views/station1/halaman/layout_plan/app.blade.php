<!DOCTYPE html>
<html lang="en">
<head>
    @include('station1.halaman.layout_plan.head')
</head>
<body class="home" style="background-color:#f9f9f9">
    @section('content')
    @show

    @include('station1.halaman.layout_plan.script')
</body>
</html>