<title>Monitoring UTR</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="Template by CocoBasic" />
<meta name="keywords" content="HTML, CSS, JavaScript, PHP" />
<meta name="author" content="CocoBasic" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="shortcut icon" href="images/favicon.ico" />       
<link href='https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,300i,400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"  href='{{asset('user/assets/css/clear.css')}}' />
<link rel="stylesheet" type="text/css"  href='{{asset('user/assets/css/common.css')}}' />
<link rel="stylesheet" type="text/css"  href='{{asset('user/assets/css/font-awesome.min.css')}}' />
<link rel="stylesheet" type="text/css"  href='{{asset('user/assets/css/flickity.min.css')}}' />
<link rel="stylesheet" type="text/css"  href='{{asset('user/assets/css/sm-clean.css')}}' />        
<link rel="stylesheet" type="text/css"  href='{{asset('user/assets/css/style.css')}}' />
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>