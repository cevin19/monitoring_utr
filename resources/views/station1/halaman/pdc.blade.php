@extends('station1.halaman.layout_plan.app')
@section('content')


<div class="container">
    <h3 class="entry-title"><a href="#">Warehouse / PDC</a></h3>
    <table class="table table-striped" style="border-radius: 10px; overflow:hidden; text-align:center; font-size:12px;">
        <thead>
            <tr style="background-color: #36304a; color:white;">
                <td>No</td>
                <td>Register</td>
                <td>Unit</td>
                <td>Komponen</td>
                <td>Date In</td>
                <td>Job</td>
                <td>Routing</td>
                <td>Start Progress</td>
                <td>Date Out</td>
                <td>Leadtime</td>
                <td>Std Leadtime</td>
                <td>Status Leadtime</td>
                <td>Plan&nbsp;Ready Go Ahead</td>
                <td>Realtime</td>
                <td>Percentage</td>

            </tr>
        </thead>

        <tbody>

            @foreach($pdcs as $pdc)

            <tr>
                <td class="serial">{{$loop->index +1}}</td>
                <td>{{$pdc->register}}</td>
                <td>{{$pdc->unit_model}}</td>
                <td>{{$pdc->nama_komponen}}</td>
                <td>{{$pdc->date_in}}</td>

                {{-- progress job --}}
                @php
                $totalurutan = count($relation_routings);
                $data_routing = App\Model\routing::where('activity',$pdc->progress_job)->get()->first();
                $data_routing = $data_routing->id;
                $data_urutan = App\Model\Relation\relation_routings::where('routing_id',$data_routing)->get()->first();
                $data_urutan = $data_urutan->urutan;

                $total_estimasi = 0;
                foreach($relation_routings as $relation_routing){
                if($relation_routing->urutan <= $data_urutan){
                    $estimasi_routing=App\Model\routing::where('id',$relation_routing->routing_id)->get()->first();
                    $total_estimasi= $total_estimasi + $estimasi_routing->estimasi;
                    }
                    }
                    if($data_urutan < $totalurutan){
                        $relation_routing_a=App\Model\Relation\relation_routings::where('urutan',$data_urutan+1)->
                        get()->first();
                        $relation_routing_b =$relation_routing_a->routing_id;
                        $data_selanjutnya = App\Model\Routing::where('id',$relation_routing_b)->get()->first();

                        }else{
                        $relation_routing_a =
                        App\Model\Relation\relation_routings::where('urutan',$data_urutan)->get()->first();
                        $relation_routing_b =$relation_routing_a->routing_id;
                        $data_selanjutnya = App\Model\Routing::where('id',$relation_routing_b)->get()->first();
                        }

                        @endphp
                        <form id="pdc-routing-update-{{$pdc->id}}" action="{{route('PDC.update',$pdc->id)}}"
                            method="POST" style="display:none">
                            @csrf @method('PATCH')
                            <input type="hidden" name="activity" id="" value="{{$data_selanjutnya->activity}}">
                        </form>
                        <td>
                            <div routing-countdownn="{{$pdc->start_progress.'/'.$total_estimasi.'/'.$pdc->id}}"></div>
                            {{$pdc->progress_job}}+ {{$total_estimasi}}
                        </td>

                        {{-- sisa routing --}}
                        @php
                        $sisa = 0;
                        foreach($relation_routings as $relation_routing){
                        if($relation_routing->urutan > $data_urutan){
                        $sisa++;
                        }
                        }
                        @endphp
                        <td id="sisa">{{$sisa}}</td>
                        <td>{{$pdc->start_progress}}</td>
                        <td>{{$pdc->date_out}}</td>

                        {{-- leadtime --}}
                        @php
                        $start_date = new DateTime($pdc->date_in);
                        $end_date = new DateTime($pdc->date_out);
                        $leadtime = $start_date->diff($end_date);
                        @endphp

                        <td>{{$leadtime->days}}</td>
                        {{-- end leadtime --}}

                        {{-- standart leadtime --}}
                        @foreach($std_leadtimes as $std_leadtime)
                        @if($pdc->nama_komponen == $std_leadtime->nama_componen)
                        <td>{{$std_leadtime->time}}</td>
                        {{-- @else
                                            <td>Belum Ada Stardart</td> --}}
                        @endif
                        @endforeach
                        {{-- status leadtime --}}
                        @foreach($std_leadtimes as $std_leadtime)
                        @if($pdc->nama_komponen == $std_leadtime->nama_componen)
                        @if($leadtime->days > $std_leadtime->time)
                        <td class="alert alert-danger" role="alert" style="color:#721c24">Over</td>
                        @elseif($pdc->date_out == null)
                        <td class="alert" role="alert" style="color:#383d41">-</td>
                        @else
                        <td class="alert alert-success" role="alert" style="color:#155724">Under</td>
                        @endif
                        @endif
                        @endforeach
                        {{-- plan ready go ahead --}}
                        @php
                        $totalurutan = count($relation_routings);
                        $data_routing = App\Model\routing::where('activity',$pdc->progress_job)->get()->first();
                        $data_routing = $data_routing->id;
                        $data_urutan =
                        App\Model\Relation\relation_routings::where('routing_id',$data_routing)->get()->first();
                        $data_urutan = $data_urutan->urutan;


                        $tambahan_waktu = 0;

                        foreach($relation_routings as $relation_routing){
                        if($relation_routing->urutan > $data_urutan){
                        $rout = App\Model\routing::where('id',$relation_routing->routing_id)->get()->first();
                        $tambahan_waktu = $tambahan_waktu + $rout->estimasi ;
                        }
                        }

                        $waktu_asal = $pdc->start_progress;
                        $carbon_date = \Carbon\Carbon::parse($waktu_asal);
                        $carbon_date->addHours($tambahan_waktu);
                        @endphp
                        <td>{{$carbon_date}}</td>

                        {{-- realtime --}}
                        <td>
                            <div data-countdown="{{$carbon_date}}"></div>
                        </td>


                        @php
                        $persentasi=substr(($data_urutan/$totalurutan)*100,0,4);
                        @endphp
                        @if ($persentasi <= 45) <td>
                            <div class="progress">
                                <div class="progress-bar bg-danger" role="progressbar"
                                    style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}"
                                    aria-valuemin="0" aria-valuemax="100">{{$persentasi}}%</div>
                            </div>
                            </td>
                            @elseif ($persentasi <= 99) <td>
                                <div class="progress">
                                    <div class="progress-bar bg-warning" role="progressbar"
                                        style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}"
                                        aria-valuemin="0" aria-valuemax="100">{{$persentasi}}%</div>
                                </div>
                                </td>
                                @elseif ($persentasi == 100)
                                <td>
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar"
                                            style="width: {{$persentasi}}%;color:white;" aria-valuenow="{{$persentasi}}"
                                            aria-valuemin="0" aria-valuemax="100">Done!</div>
                                    </div>
                                </td>
                                @endif
            </tr>
            @endforeach

        </tbody>
    </table>
</div>
<script>
    $('[data-countdown]').each(function() {
        var $this = $(this),
            finalDate = $(this).data('countdown');     
        var array_waktu=finalDate.split('|'),waktu_akhir=new Date(array_waktu[0]),pdc_id=array_waktu[1];

        var timestamp = new Date(waktu_akhir) - Date.now();
        timestamp /= 1000; // from ms to seconds

        function component(x, v) {
            return Math.floor(x / v);
        }
        setInterval(function() { // execute code each second

            timestamp--; // decrement timestamp with one second each second

            var days = component(timestamp, 24 * 60 * 60), // calculate days from timestamp
                hours = component(timestamp, 60 * 60) % 24, // hours
                minutes = component(timestamp, 60) % 60, // minutes
                seconds = component(timestamp, 1) % 60; // seconds

            if(timestamp>0){
                $this.html(days + " days, " + hours + ":" + minutes + ":" + seconds);
            }else{
                $this.html('-');
                $('#progbar-'+pdc_id).html('Done!');
            }

        }, 1000); 
    });


    $('[routing-countdownn]').each(function() {
        var $this = $(this),
            finalDate = $(this).attr('routing-countdownn');
        var array_waktu = finalDate.split('/'),
            start_progress = new Date(array_waktu[0]),
            total_estimasi = array_waktu[1],
            pdc_id = array_waktu[2];

        var waktu_akhir = (start_progress / 1000) + (total_estimasi * 3600);
        var timestamp = waktu_akhir - (Date.now() / 1000);




        function component(x, v) {
            return Math.floor(x / v);
        }
        setInterval(function() { // execute code each second

            timestamp--; // decrement timestamp with one second each second

            var days = component(timestamp, 24 * 60 * 60), // calculate days from timestamp
                hours = component(timestamp, 60 * 60) % 24, // hours
                minutes = component(timestamp, 60) % 60, // minutes
                seconds = component(timestamp, 1) % 60; // seconds


            if (timestamp < 0) {
                formsubmit(pdc_id);
            }
        }, 60000);
    });

    function formsubmit(a) {
        $('#pdc-routing-update-' + a).submit();
    }
</script>
@endsection