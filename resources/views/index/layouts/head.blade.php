<head>

    <!-- Basic Page Needs
	================================================== -->
    <meta charset="utf-8">
    <title>UTR</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
	================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#212121" />
    <meta name="msapplication-navbutton-color" content="#212121" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#212121" />

    <!-- Web Fonts 
	================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600,600i,700,700i" rel="stylesheet" />

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{asset('index/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('index/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('index/css/mind-icons-line.css')}}" />
    <link rel="stylesheet" href="{{asset('index/css/funky-style.css')}}" />
    <link rel="stylesheet" href="{{asset('index/css/owl.carousel.css')}}" />
    <link rel="stylesheet" href="{{asset('index/css/owl.transitions.css')}}" />
    <link rel="stylesheet" href="{{asset('index/css/colors/color-green.css')}}" />

    <style>
        a{
            color:#646464;
        }
        a:hover {
            text-decoration: none;
            color:#646464;
        }
    </style>
</head>