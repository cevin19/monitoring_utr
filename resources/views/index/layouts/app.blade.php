<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    @include('index.layouts.head')
</head>

<body
    style="background: url({{asset('index/img/background-train.gif')}}); background-repeat:no-repeat; background-size: 100%;background-position: 0px -150px;">
    @section('content')
    @show

    @include('index.layouts.script')
</body>

</html>