@extends('index.layouts.app')
@section('content')


<!-- Nav and Logo
	================================================== -->

<div id="menu-wrap" class="cbp-af-header white-menu-background-1st-trans menu-fixed-padding menu-shadow">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-inverse bg-faded">
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarNavMenuMain" aria-controls="navbarNavMenuMain" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">
                        <!-- <img src="{{asset('index/img/logo.png')}}" alt="" class=""> -->
                    </a>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavMenuMain">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown mega-menu active">
                                <a class="nav-link" href="#" id="navbarDropdownMenuLink-mainNav-1"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Home
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link colored-prim" href="#">
                                    About
                                </a>
                            </li>
                            <li class="nav-item icons-item-menu">
                                <a class="nav-link ml-4" href="#"><i class="fa fa-heart"></i></a>
                            </li>
                            <li class="nav-item icons-item-menu modal-search">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#Modal-search"><i
                                        class="fa fa-search"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


<!-- Search -->
<div class="modal fade default search-modal" id="Modal-search" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header justify-content-end">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="hero-center-wrap move-top">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <input type="search" value="" placeholder="Search" class="form-control" />
                                <button class="btn btn-primary btn-icon btn-round" type="submit" value="search">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







<!-- Primary Page Layout
	================================================== -->

<!-- Hero Slider Block
	================================================== -->

<div class="section big-height">
    <div class="parallax" style="background-image: url('img/parallax-hero-3.jpg')"></div>
    {{-- <div class="light-fade-over-2"></div> --}}
    <div class="hero-center-wrap z-bigger">
        <div class="container hero-text parallax-fade-top">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="typed" style="color:white; text-shadow: 2px 2px rgba(0,0,0, 0.5);">Welcome, <span
                            id="typed-1"></span></h2>
                    <div class="subtext mt-3" style="color:white; text-shadow: 2px 2px rgba(0,0,0, 0.5);">good design
                        must primarily serve people</div>
                    <a href="#content" class="btn btn-primary btn-round btn-long mt-5 scroll">Get Started</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Services Block
	================================================== -->

<div class="section pt-5 pt-md-0 pb-5 pb-md-0 background-grey z-bigger-2" id="content">
    <div class="container">
        <div class="row">
            <a href="{{route('admin.home')}}" style="display:contents">
                <div class="col-md-4 transform-y-120">
                    <div class="services-box-1 background-white drop-shadow">
                        <i class="funky-ui-icon icon-Office"></i>
                        <h5 class="color-black mt-3">Station 1</h5>
                        <p class="mt-3">Checking machine, warehouse, etc</p>
                    </div>
                </div>
            </a>
            <div class="col-md-4 transform-y-120 mt-4 mt-md-0">
                <div class="services-box-1 background-white drop-shadow">
                    <i class="funky-ui-icon icon-Split-FourSquareWindow"></i>
                    <h5 class="color-black mt-3">Station 2</h5>
                    <p class="mt-3">Coming Soon</p>
                </div>
            </div>
            <div class="col-md-4 transform-y-120 mt-4 mt-md-0">
                <div class="services-box-1 background-white drop-shadow">
                    <i class="funky-ui-icon icon-Hammer"></i>
                    <h5 class="color-black mt-3">Station 3</h5>
                    <p class="mt-3">Coming Soon</p>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 transform-y-120">
                <div class="services-box-1 background-white drop-shadow">
                    <i class="funky-ui-icon icon-Robot"></i>
                    <h5 class="color-black mt-3">Station 4</h5>
                    <p class="mt-3">Checking machine, warehouse, etc</p>
                </div>
            </div>
            <div class="col-md-4 transform-y-120 mt-4 mt-md-0">
                <div class="services-box-1 background-white drop-shadow">
                    <i class="funky-ui-icon icon-Road"></i>
                    <h5 class="color-black mt-3">Station 5</h5>
                    <p class="mt-3">Coming Soon</p>
                </div>
            </div>
            <div class="col-md-4 transform-y-120 mt-4 mt-md-0">
                <div class="services-box-1 background-white drop-shadow">
                    <i class="funky-ui-icon icon-Hamburger"></i>
                    <h5 class="color-black mt-3">Station 6</h5>
                    <p class="mt-3">Coming Soon</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Quotes Block
	================================================== -->

<div class="section padding-bottom background-grey">
    <div class="container">
        <div class="row justify-content-center" data-scroll-reveal="enter bottom move 40px over 0.8s after 0.2s">
            <div class="col-md-8">
                <div class="quote-box-1 text-center">
                    <h5 class="mb-5">"Life is short. Smile while you have teeth"</h5>
                    <h4>Daffa Alvi</h4>
                    <p class="mt-2">Google</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Separator Line
	================================================== -->

<div class="section padding-top-bottom-1 background-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="separator-wrap">
                    <span class="separator"><span class="separator-line dashed"></span></span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Logos Block
	================================================== -->

<div class="section padding-top-bottom-small background-grey over-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a href="#">
                    <img src="img/logos/d1.png" class="img-120 mx-auto" alt="" />
                </a>
            </div>
            <div class="col-md-2 mt-4 mt-md-0">
                <a href="#">
                    <img src="img/logos/d2.png" class="img-120 mx-auto" alt="" />
                </a>
            </div>
            <div class="col-md-2 mt-4 mt-md-0">
                <a href="#">
                    <img src="img/logos/d5.png" class="img-120 mx-auto" alt="" />
                </a>
            </div>
            <div class="col-md-2 mt-4 mt-md-0">
                <a href="#">
                    <img src="img/logos/d6.png" class="img-120 mx-auto" alt="" />
                </a>
            </div>
            <div class="col-md-2 mt-4 mt-md-0">
                <a href="#">
                    <img src="img/logos/d7.png" class="img-120 mx-auto" alt="" />
                </a>
            </div>
            <div class="col-md-2 mt-4 mt-md-0">
                <a href="#">
                    <img src="img/logos/d11.png" class="img-120 mx-auto" alt="" />
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Footer Short Light Block
	================================================== -->

<div class="section background-black over-hide footer-1 dark">
    <div class="container scd-foot pt-4 pb-4">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="mb-0">© 2019 UTR. Powered with <i class="fa fa-heart"></i> By InfoKN</p>
            </div>
        </div>
    </div>
</div>


<div class="scroll-to-top"><i class="fa fa-angle-up"></i></div>
@endsection